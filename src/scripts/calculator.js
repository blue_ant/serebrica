const $priceInput  = $('#calc-flat-price');
const $priceSlider = $('#calc-flat-price-slider');
const $priceOrder  = $('#calc-order-price');

const $payInput   = $('#calc-first-pay');
const $paySlider  = $('#calc-first-pay-slider');
const $payPercent = $('#calc-first-pay-percent');
const $payOrder   = $('#calc-order-first-pay');

const $amountInput  = $('#calc-credit-amount');
const $amountSlider = $('#calc-credit-amount-slider');
const $amountOrder  = $('#calc-order-credit-amount');

const $periodInput  = $('#calc-credit-period');
const $periodSlider = $('#calc-credit-period-slider');
const $periodOrder  = $('#calc-order-credit-period');

const $ageInput  = $('#calc-user-age');
const $ageSlider = $('#calc-user-age-slider');
const $ageOrder  = $('#calc-order-user-age');

function amount2Word(amount, words) {
  var resOfHundred = amount % 100;
  var restOfTen    = amount % 10;
  var resultString;

  switch (true) {
    case (resOfHundred >= 5 && resOfHundred <= 20):
      resultString = ' ' + words[2];

      break;

    default:
      switch (true) {
        case (restOfTen == 1):
          resultString = ' ' + words[0];

          break;
        case (restOfTen >= 2 && restOfTen <= 4):
          resultString = ' ' + words[1];

          break;
        default:
          resultString = ' ' + words[2];

          break;
      }
      break;
  }
  return (resultString);
}

function PrintElem($elem) {
  var mywindow = window.open('', 'PrintWindow');

  mywindow.document.write('<html><head><title>' + document.title  + '</title>');
  mywindow.document.write(
    `<style>
      @page {
        size: A4;
        margin: 2mm;
      }

      body, html {
        padding: 0;
      }

      p {
        margin-bottom: 40px;
        text-align: center;
      }

      .calculator-print {
        display: none;
        padding: 0;
        margin: 0;
      }

      .calculator-print__header {
        display: table;
        background: #ededed;
        padding: 20px;
        border: solid 1px #eeeeee;
        margin-bottom: 40px;
        width: 100%;
      }

      .calculator-print__item {
        display: table-cell;
        text-align: center;
        background: #ededed;
        padding: 0;
      }

      .calculator-print__img {
        display: block;
        width: 150px;
        margin: 0 auto 50px;
      }

      .calculator-print__body {
        width: 100%;
        padding: 0 20px;
      }

      .calculator-print__body td,
      .calculator-print__body th {
        text-align: left;
        padding: 10px 0;
        border-bottom: solid 1px #eeeeee;
      }
    </style>`
  );
  mywindow.document.write('</head><body>');
  mywindow.document.write($elem.html());
  mywindow.document.write('</body></html>');

  mywindow.document.close();
  mywindow.focus();

  mywindow.print();
  mywindow.close();

  return true;
}

function emailElem(innerHtml) {
  var template =
  `<html>
    <head>
      <style>
        body, html {
          padding: 0;
        }

        p {
          margin-bottom: 40px;
          text-align: center;
        }

        .calculator-print {
          display: none;
          padding: 0;
          margin: 0;
        }

        .calculator-print__header {
          display: table;
          background: #ededed;
          padding: 20px;
          border: solid 1px #eeeeee;
          margin-bottom: 40px;
          width: 90%;
        }

        .calculator-print__item {
          display: table-cell;
          text-align: center;
          background: #ededed;
          padding: 0;
        }

        .calculator-print__img {
          display: block;
          width: 150px;
          margin: 0 auto 50px;
        }

        .calculator-print__body {
          width: 95%;
          padding: 0 20px;
        }

        .calculator-print__body td,
        .calculator-print__body th {
          text-align: left;
          padding: 10px 0;
          border-bottom: solid 1px #eeeeee;
        }
      </style>
    </head>
    <body>` + innerHtml + `</body>
  </html>`;

  return template;
}

class Calculator {
  constructor() {
    let $params   = $('[data-calculator-params]');
    let $submit   = $('[data-calculator-submit]');
    let $print    = $('[data-calculator-print]');
    let $showMore = $('[data-show-more]');
    let $form     = $('[data-calc-form]');
    let $email    = $('[data-calculator-email]');

    this.paramsInit($params);
    this.calculate($submit, $print);
    this.print($print);
    this.showMore($showMore);
    this.formInit($form);
    this.emailInit($email);
  }

  emailInit($email) {
    $email.on('submit', function(e) {
      e.preventDefault();

      let $this    = $(this);
      let $wrapper = $this.closest('.calculator-email');
      let url      = $this.attr('action');
      let method   = $this.attr('method');
      let calcData = $('[data-calculator-print]').html();
      let data     = emailElem(calcData);
      let sendData = $this.serialize() + '&text=' + data.replace(/&nbsp;/g, ' ');

      $.ajax({
        url: url,
        method: method,
        data: sendData,
        success: function(response) {
          $wrapper.find('.calculator-email__tlt').text(response.title)
          $wrapper.find('p').text(response.text);
          $this.hide();
        }
      })
    });
  }

  formInit($form) {
    $form
      .css('height', $form.get(0).scrollHeight)
      .on('submit', function(e) {
        let $this     = $(this);
        let curHeight = $this.outerHeight();
        let data      = $this.serialize();
        let url       = $this.attr('action');
        let method    = $this.attr('method');

        e.preventDefault();

        $.ajax({
          url: url,
          method: method,
          data: data,
          success: function(response) {
            $this
              .stop()
              .animate({
                height: 200
              }, function() {
                $this.replaceWith('<div>' + response.success + '</div>');
              });
          }
        });
    });
  }

  showMore($showMore) {
    var $result = $('.calculator-result');

    $showMore.on('click', function(e) {
      let $this = $(this);
      let active = '_active';

      if ($this.hasClass(active)) {
        $this
          .removeClass(active)
          .text('Показать больше банков');

        $result.stop().animate({
          height: 549
        });

        return;
      }

      $this
        .addClass(active)
        .text('Скрыть');

      $result.stop().animate({
        height: $('.calculator-result__inner').get(0).scrollHeight
      });
    });
  }

  print($print) {
    let $printBtn = $('[data-calculator-print-btn]');

    $printBtn.on('click', function(e) {
      e.preventDefault();

      let $print = $('[data-calculator-print]');

      PrintElem($print);
    });
  }

  paramsInit($params) {
    let $items = $params.find('[data-params-item]');

    $items.each((i, e) => {
      let $item   = $(e);
      let $slider = $item.find('[data-params-slider]');
      let $input  = $item.find('[data-params-input]');
      let data    = $slider.data('params-slider');

      this.sliderInit($item, $slider, data);
    });
  }

  inputInit($wrapper, $input, data) {
    var $slider = $wrapper.find('[data-params-slider]');
    var dataMin = data.min;
    var dataMax = data.max;

    $input.on('focus', function(e) {
      let $this = $(this);
      let value = $this.val().replace(/\D/g,'');

      $this.val(value);
    });

    $input.on('keydown', function(e) {
      if ($.inArray(e.keyCode, [9, 27, 13, 110, 190]) !== -1 ||
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 40)) {
          $(this).blur();

          return;
      }

      if ( $.inArray(e.keyCode, [46, 8]) === -1 && (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ) {
        e.preventDefault();

        return;
      }
    });

    $input.on('keyup', function(e) {
      let $this = $(this);
      let value = $this.val().replace(/\D/g,'');

      $slider.slider('option', 'value', value);
    });

    $input.on('blur', function() {
      let $this = $(this);
      let value = $this.val();

      if ($this.is('#calc-first-pay')) {
        dataMin = Math.round($priceInput.data('val') * .1);
        dataMax = Math.round($priceInput.data('val') * .85);
      }

      if (value < dataMin) {
        value = dataMin;
      } else if (value > dataMax) {
        value = dataMax;
      }

      if ($this.is('#calc-flat-price')) {
        $paySlider.slider('option', 'min', Math.round(value * .1));
        $paySlider.slider('option', 'max', Math.round(value * .85));
      }

      $this
        .val(Number(value).toLocaleString("ru-RU") + ' Р')
        .data('val', value);

      $payPercent.val( ($payInput.data('val') / $priceInput.data('val') * 100).toFixed(0) + '%' );

      let amount = $priceInput.data('val') - $payInput.data('val');

      if (amount < 0) {
        amount = 0;
      }

      $amountInput.val(Number(amount).toLocaleString('ru-RU') + ' Р');
      $amountSlider.slider('option', 'value', amount);
      $amountSlider.slider('option', 'max', amount);

      $amountOrder
        .val(amount)
        .data('val', amount);
    });
  }

  sliderInit($wrapper, $slider, data) {
    let disabled = data.disabled;
    let min  = data.min;
    let max  = data.max;
    let step = data.step;
    let val  = data.val;
    let type = data.type;

    let $input = $wrapper.find('[data-params-input]');
    let ageWord = amount2Word(val, ['год', 'года', 'лет']);

    let rouble = (type === 'price' || type === 'payment' || type === 'amount') ? ' Р' : (' ' + ageWord);

    $input
      .val(Number(val).toLocaleString('ru-RU') + rouble)
      .data('val', val);

    $priceOrder.val($priceInput.data('val'));
    $payOrder.val($payInput.data('val'));
    $amountOrder.val($amountInput.data('val'));
    $periodOrder.val($periodInput.data('val'));
    $ageOrder.val($ageInput.data('val'));

    $payPercent.val($payInput.data('val') / $priceInput.data('val') * 100 + '%');

    $slider.slider({
      min: min,
      max: max,
      disabled: disabled,
      step: step,
      value: val,
      classes: {
        'ui-slider': 'calculator-slider',
        'ui-slider-handle': 'calculator-slider__handle',
        'ui-slider-range': 'calculator-slider__range'
      },
      range: 'min',
      slide: function(e, ui) {
        let curValue = ui.value;

        ageWord = amount2Word(curValue, ['год', 'года', 'лет']);

        $input
          .val(Number(curValue).toLocaleString('ru-RU') + rouble)
          .data('val', curValue);

        if (type === 'price') {
          if (curValue * .85 < $paySlider.slider('option', 'value')) {
            $paySlider.slider('option', 'value', Math.round(curValue * .85));
            $payInput
              .val(Math.round(curValue * .85).toLocaleString('ru-RU') + rouble)
              .data('val', curValue * .85);
          }

          $paySlider.slider('option', 'max', Math.round(curValue * .85));
          $paySlider.slider('option', 'min', Math.round(curValue * .1));
          $priceOrder
            .val($priceInput.data('val'))
            .data('val', Math.round(curValue * .85));

          if (curValue * .1 > $payInput.data('val')) {
            $paySlider.slider('option', 'value', Math.round(curValue * .1));
            $payInput
              .val(Math.round(curValue * .1).toLocaleString('ru-RU') + rouble)
              .data('val', Math.round(curValue * .1));
          }
        }

        if (type === 'payment') {
          $payOrder.val($payInput.data('val'));
        }

        if (type === 'price' || type === 'payment') {
          let amount = $priceInput.data('val') - $payInput.data('val');

          if (amount < 0) {
            amount = 0;
          }

          $payPercent.val( Math.round($payInput.data('val') / $priceInput.data('val') * 100) + '%' );

          $amountInput
            .val(Number(amount).toLocaleString('ru-RU') + rouble)
            .data('val', amount);
          $amountSlider.slider('option', 'value', amount);
          $amountSlider.slider('option', 'max', amount);

          $amountOrder.val(amount);
        }

        if (type === 'period') {
          $periodInput
            .val(Number(curValue).toLocaleString('ru-RU') + (' ' + ageWord))
            .data('val', curValue);

          $periodOrder.val($periodInput.data('val'));
        } else if (type === 'age') {
          $ageInput
            .val(Number(curValue).toLocaleString('ru-RU') + (' ' + ageWord))
            .data('val', curValue);

          $ageOrder.val($ageInput.data('val'));
        }
      }
    });

    if (!$input[0].hasAttribute('readonly')) {
      this.inputInit($wrapper, $input, data);
    }
  }

  calculate($submit, $print) {
    $submit.on('click', function(e) {
      e.preventDefault();

      var housingComplexId = '630';
      var agendaType       = 'primary_housing';
      var isMilitaryGrade  = 'regular';
      var isInsured        = 'true';
      var proofOfIncome    = 'ndfl';
      var isRfCitizen      = 'true';
      var cost             = $priceInput.data('val') * 100;
      var initialPayment   = $payInput.data('val') * 100;
      var age              = $ageInput.data('val');
      var credit_time      = $periodInput.data('val') * 12;

      var url = 'https://gql.ipoteka.digital/gql?query={getLoanOffer(housingComplexId:' + housingComplexId + 'agendaType:' + agendaType + ',cost:' + cost + ',initialPayment:' + initialPayment + ',isMilitaryGrade:' + isMilitaryGrade + ',isInsured:' + isInsured + ',proofOfIncome:' + proofOfIncome + ',isRfCitizen:' + isRfCitizen + ',age:' + age + '){id,bankName,bankLogo,periods{period,amount},recommendedIncomeCoeff,rate}}';

      let $result = $('[data-calc-result]');
      let active  = '_active';

      $result.removeClass('_empty _short').find('.calculator-result__inner').empty();
      $result.addClass(active).find('.Preloader_pic').show();
      $('[data-calc-show]').addClass(active);

      let $printHeader = $print.find('.calculator-print__header');
      let $printBody   = $print.find('.calculator-print__tbody');

      $printBody.empty();
      $printHeader.empty().append(
        `<div class="calculator-print__item">` + $priceInput.data('val').toLocaleString('ru-RU') + ` руб.<br>Стоимость недвижимости</div>
          <div class="calculator-print__item">` + $payInput.data('val').toLocaleString('ru-RU') + ` руб.<br>Первоначальный взнос</div>
          <div class="calculator-print__item">` + $amountInput.data('val').toLocaleString('ru-RU') + ` руб.<br>Сумма кредита</div>
          <div class="calculator-print__item">` + $periodInput.data('val') + amount2Word($periodInput.data('val'), ['год', 'года', 'лет']) + `<br>Срок ипотеки</div>
          <div class="calculator-print__item">` + $ageInput.data('val') + amount2Word($ageInput.data('val'), ['год', 'года', 'лет']) + `<br>Возраст заемщика</div>
        `);

      $.getJSON(url, function(data) {
        var banks_json = data;

        function getUnique(arr, comp) {
          const unique = arr
            .map(e => e[comp])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter(e => arr[e]).map(e => arr[e]);

            return unique;
        }

        function GetSortOrder(prop) {
          return function(a, b) {
            if (a[prop] > b[prop]) {
              return 1;
            } else if (a[prop] < b[prop]) {
              return -1;
            }

            return 0;
          }
        }

        var banks_arr = banks_json.data.getLoanOffer;

        banks_arr = banks_arr.sort(GetSortOrder("rate"));
        banks_arr = getUnique(banks_arr,'bankName');

        banks_arr.forEach(function(element) {
          var bank_name         = element.bankName;
          var bank_rate         = element.rate;
          var bank_periods      = element.periods;
          var bank_month_amount = 0;
          var bank_logo         = element.bankLogo;

          bank_periods.forEach(function(bank_period) {
            var period_months = bank_period.period;
            var period_amount = Math.ceil(bank_period.amount / 100).toLocaleString('ru-RU');
            var period = period_months / 12 + amount2Word(period_months / 12, ['год', 'года', 'лет']);
            var template = `<div class="calculator-result__item calculator-result-item">
                <div class="calculator-result-item__td">
                  <img class="calculator-result-item__img" src="` + bank_logo + `" alt="">
                </div>
                <div class="calculator-result-item__td">
                  <div class="calculator-result-item__val">` + period + `</div>
                  <div class="calculator-result-item__key">Срок до</div>
                </div>
                <div class="calculator-result-item__td">
                  <div class="calculator-result-item__val">` + bank_rate + `%</div>
                  <div class="calculator-result-item__key">Ставка</div>
                </div>
                <div class="calculator-result-item__td">
                  <div class="calculator-result-item__val">` + period_amount + ` Р</div>
                  <div class="calculator-result-item__key">Платеж в месяц</div>
                </div>
              </div>`;

            var printTplInner = `<tr>
                              <td>` + bank_name + `</td>
                              <td>` + bank_rate + `&nbsp;%</td>
                              <td>` + period + `</td>
                              <td>` + period_amount + `&nbsp;руб.</td>
                            </tr>`;

            if (credit_time == period_months) {
              $result.find('.calculator-result__inner').append(template);
              $printBody.append(printTplInner);
            }
          });
        });

        $result.find('.Preloader_pic').hide();

        let calcItems = $('.calculator-result-item').length;

        if (calcItems < 5)
          $result.addClass('_short');

        if (calcItems == 0) {
          let emptyTpl = `<div class="calculator-result__empty">Извините! Такой программы не существует. Попробуйте изменить параметры кредита.</div>`;

          $result
            .addClass('_empty')
            .find('.calculator-result__inner')
            .append(emptyTpl);
        }
      });
    });
  }
}

new Calculator();

$(document.querySelector(".BankList_showMore")).one("click", event => {
    event.preventDefault();
  window.requestAnimationFrame(() => {
    $(".BankList_tblBody").removeClass("BankList_tblBody-closed");
  $(event.currentTarget)
    .parent()
    .remove();
  });
});
