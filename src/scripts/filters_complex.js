let popupTpl = _.template(
	`<div class="Dialog Dialog-complex" style="width: 544px; pointer-event:none;">
        <div class="Dialog_head"><%= title %></div>
        <div class="Dialog_content">
            <div class="Dialog_leftBlock" style="background-image: url('<%= plan %>')"></div>
            <div class="Dialog_rightBlock">
                <dl class="Dialog_dl">
                    <dt class="Dialog_dt">Площадь:</dt>
                    <dd class="Dialog_dd"><%= footage %> м<sup>2</sup></dd>
                    <dt class="Dialog_dt">Цена:</dt>
                    <dd class="Dialog_dd"><%= price.toLocaleString("Ru-ru") %>&nbsp;₽</dd>
                    <% if(promoPrice){ %>
                        <dt class="Dialog_dt">Цена по акции:</dt>
                        <dd class="Dialog_dd Dialog_dd-pricePromo"><%= promoPrice.toLocaleString("Ru-ru") %>&nbsp;₽</dd>
                    <% }; %>
                    <dt class="Dialog_dt">Отделка:</dt>
                    <dd class="Dialog_dd">
                        <% if(furnish){ %>
                            да
                        <% } else{ %>
                            нет
                        <% }; %>
                    </dd>
                </dl>
            </div>
        </div>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {
	let $scheme = $(".FilterComplex_sectionScheme");

	if (currentDevice.desktop() && $scheme.length) {
		$scheme.tooltip({
			items: "[data-params]",
			track: true,
			hide: { duration: 0 },
			show: { duration: 0 },
			content: function() {
				let $el = $(this);
				let params = $el.data("params");
				let dataToRender = {
					plan: null,
					title: null,
					price: null,
					footage: null,
					promoPrice: null,
					entryDate: null,
					furnish: null,
				};
				_.assignIn(dataToRender, params);
				return popupTpl(dataToRender);
			},
		});
	}
};

$(window).on("load", () => {
	setupSectionHoverBehaviour();
});

$("main").on("click", ".FloorBuildNav_link", (event) => {
	event.preventDefault();
	let ref = event.currentTarget.href;
	let $schemeBlock = $(".FilterComplex");
	$schemeBlock.addClass("FilterComplex-disabled");
	$.ajax({
		url: ref,
		dataType: "html",
	})
		.done((response) => {
			let $newDoc = $(response);

			let $newSectionInfo = $newDoc.find(".FilterComplex");
			$schemeBlock.html($newSectionInfo.html());
			setupSectionHoverBehaviour();

			let title = $newDoc.filter("title").html();
			document.title = title;
			history.replaceState({}, title, ref);

			const $checkbox = $(".CustomCheckbox").find('[name="bedrooms[]"]');

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggerfilter=${data}]`).addClass("ColorLegend_item-activeFilter");
				}
			});

			colorLegendTrigger.init();
		})
		.fail(function() {
			alert("Не удалось получить данные о секции!/nПопробуйте позже.");
		})
		.always(function() {
			$schemeBlock.removeClass("FilterComplex-disabled");
		});
});

let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find("#FilterSidebarResetFull").on("click", (event) => {
	event.preventDefault();
	filter.resetFilterForm();
	colorLegendTrigger.reset();
	CorpusSchemeTrigger.reset();
});

sidebar.$el.find("#FilterSidebarResetGroup").on("click", (event) => {
	event.preventDefault();
	filter.resetChecksGroup();
});

$(".FilterMobileSwitcher").selectmenu({
	change: (event, ui) => {
		window.location.href = ui.item.value;
	},
});

let filterParams = {
	submitHandler: (filterInst) => {
		let $filterForm = filterInst.$filterForm;
		let dataToSend = $filterForm.serializeObject();

		console.log(dataToSend);
		$.extend(true, dataToSend, {
			action: "get_flats",
            ajax: "y"
		});

		$.ajax({
			url: $filterForm.attr("action") || $filterForm.data("action"),
			dataType: "json",
			method: "GET",
			beforeSend: function() {
				if(window.pagePreloader.show) {
					window.pagePreloader.show();
				}
			},
			complete: function() {
				if (window.pagePreloader.hide) {
					window.pagePreloader.hide();
				}
			},
			data: dataToSend,
		})
			.done((response) => {
				if ( response.bld_floor_ids ) {
					$(".bld-flat-hover").each(function(){
						var flat_id = $(this).data("flat_id");

						if ( response.bld_floor_ids.indexOf( flat_id ) == -1 ) {
							$(this).addClass("hide-flat");
						} else
							$(this).removeClass("hide-flat");
					})
				}
			})
			.fail(() => {
				alert("Не удалось получить данные с сервера! Попробуйте позже.");
			})
			.always(function() {
				// window.pagePreloader.hide();
			});
	},
};

let filter = new FilterForm("#filterFormHorizon", filterParams);

//Привязка цветовых обозначений к фильтру

let colorLegendTrigger = {
	init: function() {
		const $item = $(".ColorLegend_item");
		const $checkbox = $(".CustomCheckbox").find('[name="bedrooms[]"]');

		$item.on("click", function(e) {
			const $data = $(e.currentTarget).data("triggerfilter");
			console.log($data)
			$checkbox.each((i, e) => {
				if ($(e).data(`persist-id`) === $data) {
					$(e).trigger("click");

					if ($(e).is(":checked")) {
						$(e.currentTarget).addClass("ColorLegend_item-activeFilter");
					} else {
						$(e.currentTarget).removeClass("ColorLegend_item-activeFilter");
					}
				}
			})
		});

		$checkbox.on("change", function() {
			const data = $(this).data("persist-id");

			if ($(this).is(":checked")) {
				$(`[data-triggerfilter=${data}]`).addClass("ColorLegend_item-activeFilter");
			} else {
				$(`[data-triggerfilter=${data}]`).removeClass("ColorLegend_item-activeFilter");
			}
		});

		$(window).on("load", function() {
			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggerfilter=${data}]`).addClass("ColorLegend_item-activeFilter");
				}
			});
		});
	},

	reset: function() {
		const $item = $(".ColorLegend_item");

		$item.removeClass("ColorLegend_item-activeFilter");
	}
};

colorLegendTrigger.init();

let isRadioCorpus = true;
let CorpusSchemeTrigger = {
	init: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		$item.on("click", function(e) {
			const $data = $(e.currentTarget).data("triggercorpus");
			e.preventDefault()
			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			$checkbox.each((i, e) => {
				if ($(e).data(`persist-id`) === $data) {
					$(e).trigger("click");

					if ($(e).is(":checked")) {
						$(e.currentTarget).addClass("SvgSectionNav_link-active");
					} else {
						$(e.currentTarget).removeClass("SvgSectionNav_link-active");
					}
				}
			})
		});

		$checkbox.on("change", function() {
			const data = $(this).data("persist-id");

			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			
			if ($(this).is(":checked")) {
				$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
			} else {
				$(`[data-triggercorpus=${data}]`).removeClass("SvgSectionNav_link-active");
			}
		});

		$(window).on("load", function() {

			if (isRadioCorpus) {
				$checkbox.find('[data-persist-id = "item_1"]').trigger('click');
			}

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		});
	},

	reset: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		if (isRadioCorpus) {
			$checkbox.find('[data-persist-id = "item_1"]').trigger('click');

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");
					$item.removeClass("SvgSectionNav_link-active");
					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		}
	}
};

CorpusSchemeTrigger.init();

if (device.default.type !== 'desktop' && window.REDIRECT_PATH) {
	window.location.href = window.REDIRECT_PATH;
}