/*=require ./includes/chunks/APP_CONFIGS.js */
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/blocks/*.js */

function getWindowSizeBreakpoint() {
	let w = window.innerWidth;
	let result = "md";
	if (w < 768) {
		result = "xs";
	} else if (w > 767 && w < 1440) {
		result = "sm";
	}
	return result;
}

function prepareFilterFormValues(valsObj) {
	return _.mapValues(valsObj, param => {
		// convert all strings and arrays from inputs to 'INT' type
		let result;
		if (_.isString(param)) {
			result = parseInt(param);
		} else if (_.isArray(param)) {
			result = _.map(param, p => {
				return parseInt(p);
			});
		}
		return result;
	});
}

const currentDevice = device.default;

let menuBurger = new CustomBurger(".Header_menuBtn");
let mainMenu = new MainMenu(".Menu", {
	onShow: () => {
		menuBurger.drawClosedBurger();
	},
	onHide: () => {
		menuBurger.reset();
	}
});

$(".Header_menuBtn").on("click", event => {
	event.preventDefault();
	mainMenu.show();
	return false;
});

new InteractiveForm($("#orderModal").find(".Form"));

let $window = $(window);

$(".Teaser").each((index, el) => {
	new Teaser(el);
});

let loadedPage = false;

$window.on("load", () => {
	if ($window.scrollTop() < 100) {
		new WOW().init({
			live: false
		});
	}
	{
		let $PopupBtn = $(".PopupBtn");
		if ($PopupBtn.length) {
			$PopupBtn.each((i, e) => {
				new PopupBtn(e);
			})
		}

		let $bird = $(".Bird");

		if ($bird.length) {
			if ($PopupBtn.length) {
				let birdFly = new Bird($PopupBtn);
				birdFly.startAnimation();
			} else {
				$bird.hide();
				console.error("Bird's target element not found in DOM!");
			}
		}
	}
	loadedPage = true;
	window.pagePreloader.hide();

	new BackToTop("#backtotop");
});

window.pagePreloader = new Preloader("#pagePreloader");

if ($('.ArticleBurger').length) {
	const articleBurger = new ArticleBurger(document.querySelector('.ArticleBurger'))
}
