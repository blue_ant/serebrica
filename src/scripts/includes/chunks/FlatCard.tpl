<% 
  let promoCardsOrders = [];
  promos.forEach(function(promoCard){
	promoCardsOrders.push(promoCard.order - 1);
  });

  var eachFlatCardIteration = 0;
%>
<% _.forEach(plans, function(group, rooms) { %>
<h4 class="CardsList_groupTitle">

		<% if(rooms == 0){%>
			Студия
		<% } else{ %>
			<%= rooms %> <%= declOfNum(rooms, ['комната','комнаты','комнат']) %>
		<% }; %>
		: <%= group.length %> <%= declOfNum(group.length, ['тип','типа','типов']) %>
		
	</h4>
<div class="CardsList_groupList">
	<% _.forEach(group, function(card, index) { %>
		<% if(_.includes(promoCardsOrders, eachFlatCardIteration)){ %>
			<% let promo = promos.shift();%>
			<div class="FlatCard FlatCard-promo">
				<div class="FlatCard_promoInner">
					<div class="FlatCard_discountVal">
						<%= promo.headTopText %>
					</div>
					<h6 class="FlatCard_promoHead"><%= promo.title %></h6>
					<div class="FlatCard_promoText">
						<%= promo.desc %>
					</div><a class="Btn Btn-color3 Btn-size2" href="<%= promo.link %>">Узнать подробнее</a>
				</div>
			</div>
			<% eachFlatCardIteration++ %>
		<% }; %>
		<% let plan = card; %>
		<a class="FlatCard" href="<%= plan.href %>">
			<div class="FlatCard_head"><%= plan.name %></div>
			<div class="FlatCard_content">
				<div class="FlatCard_schemeWrap"><img class="FlatCard_scheme" src="<%= plan.plan %>" alt=""></div>
				<div class="FlatCard_paramsWrap">
					<div class="FlatCard_leafLine"></div>
					<table class="FlatCard_paramsTbl">
						<tbody>
							<tr class="FlatCard_paramsRow">
								<td class="FlatCard_paramsCell"><span class="FlatCard_paramsLabel">площадь от</span></td>
								<td class="FlatCard_paramsCell"><span class="FlatCard_paramsLabel">цена от</span></td>
							</tr>

							<tr class="FlatCard_paramsRow">
								<td class="FlatCard_paramsCell"><span class="FlatCard_paramsVal"><%= plan.areaMin %></span></td>
								<td class="FlatCard_paramsCell"><span class="FlatCard_paramsVal"><%= plan.priceMin.toLocaleString("Ru-ru") %> ₽</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</a>
		<% eachFlatCardIteration++ %>
	<% }); %>
</div>
<% }); %>