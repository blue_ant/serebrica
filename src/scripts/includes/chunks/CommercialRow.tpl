<%
let okIco = '<svg class="SvgIco SvgIco-checkBold" width="12" height="9" xmlns="http://www.w3.org/2000/svg">
			<path class="SvgIco_path" d="M10.071 0l1.768 1.767-7.071 7.071L3 7.07z"></path>
			<path class="SvgIco_path" d="M.491 4.509L2.26 2.74l3.567 3.567L4.06 8.076z"></path>
		</svg>';
let crossIco = '<svg class="SvgIco SvgIco-crossBold" width="10" height="10" xmlns="http://www.w3.org/2000/svg"><path class="SvgIco_line" stroke-width="2" d="M1 9l8-8M1 1l8 8"></path></svg>'
%>
<% _.forEach(flats, function(flt) { %>
<a class="Tbl_tr Tbl_tr-link" href="<%= flt.url %>">
	<div class="Tbl_td hidden-xs">
		<div class="Tbl_thumb" style="background-image:url('<%= flt.pic %>');" data-full-img="<%= flt.pic %>"></div>
	</div>
	<div class="Tbl_td"><%= flt.area %> <span class="hidden-xs">м<sup>2</sup></span></div>
	<div class="Tbl_td">Корпус <%= flt.section %></div>
	<div class="Tbl_td"><%= flt.bldYear %><br/><span class="Tbl_bldTime"><%= flt.bldTime %> квартал</span></div>
	<div class="Tbl_td"><%= flt.status %></div>
	
	<% if(flt.oldPrice){ %>
		<div class="Tbl_td Tbl_td-nowrap">
			<del class="Tbl_delText"><%= flt.oldPrice.toLocaleString("ru-RU") %> ₽</del>
			<br>
			<ins class="Tbl_insText"><%= flt.price.toLocaleString("ru-RU") %> ₽</ins>
		</div>
	<% }else{ %>
		<div class="Tbl_td Tbl_td-nowrap"><%= flt.price.toLocaleString("ru-RU") %> ₽</div>
	<% }; %>
</a>
<% }); %>