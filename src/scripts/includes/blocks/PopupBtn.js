class PopupBtn {
	constructor(element) {
		let $el = $(element);
		this.tl = null;
		this.$content = $el.find(".PopupBtn_content");
		this.$btn = $el.find(".PopupBtn_btn");
		this.isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

		this.isArticleBurger = $el.hasClass('PopupBtn-articleBurger');

		if (!this.isFirefox) {
			$(window).on("resize.PopupBtn", _.debounce(this.initAnimate.bind(this), 100));
			this.initAnimate();
		} else {
			$el.addClass('PopupBtn-fierfox')
		}
		this.$btn.on("click", () => {
			if (!this.$btn.hasClass("PopupBtn_btn-open")) {
				this.$btn.addClass("PopupBtn_btn-open");
				if (!this.isFirefox) {
					this.tl.play();
				} else {
					this.$content.fadeIn(400);
				}
			} else {
				this.$btn.removeClass("PopupBtn_btn-open");
				if (!this.isFirefox) {
					this.tl.reverse();
				} else {
					this.$content.fadeOut(400);
				}
			}
		});

		this.$el = $el;

		if (this.isArticleBurger) {
			this.scrollLinks()
		}
	}

	initAnimate() {
		let ended = false;
		if (this.tl != null && this.tl.progress() == 1) {
			ended = true;
		}
		this.$content.show()
		this.tl = new TimelineLite({ paused: true });
		let w = this.$content.width();
		let h = this.$content.height();
		this.tl
			.fromTo(
				this.$content,
				0.3,
				{
					clip: `rect(0px, ${w}px, 0px, ${w - 10}px)`
				},
				{
					clip: `rect(0px, ${w}px, ${h}px, ${w - 10}px)`
				}
			)
			.to(this.$content, 0.3, {
				clip: `rect(0px, ${w}px, ${h}px, 0px)`
			})
			.fromTo(this.$content.children(), 0.3, { opacity: 0 }, { opacity: 1 }, "-=0.3");
		if (ended) {
			this.tl.progress(1);
		}
	}

	scrollLinks() {
		const links = this.$el.find('[data-scrollTo]');

		links.on('click', function (e) {
			e.preventDefault();

			const dataLink = $(this).data('scrollto');
			const title = $(document).find(`[data-titlescroll="${dataLink}"]`);

			$('html, body').animate({
				scrollTop: title.offset().top
			}, 2000);
		})
	}
}
