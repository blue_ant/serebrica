function CustomMarker(map, icon, content, coord, isNeedHover = false) {
	this.map_ = map;
	this.isHover = isNeedHover;
	this.icon_ = icon;
	this.content_ = content;
	this.coord_ = coord;
	this.div_ = null;
	this.draw = () => {
		const overlayProjection = this.getProjection();
		let position = overlayProjection.fromLatLngToDivPixel(this.coord_);
		this.div_.style.left = position.x - 30 + "px";
		this.div_.style.top = position.y - 60 + "px";
	};
	this.onAdd = () => {
		const div = document.createElement("div");
		div.classList.add("marker");
		if (this.isHover) {
			div.classList.add("marker-hover");
		}
		div.innerHTML = `<div class="marker_ico" style="background-image: url(${
			this.icon_
		})"></div><div class="marker_textWrp"><div class="marker_text"><span>${this.content_}</span></div></div>`;
		const marker = this;
		google.maps.event.addDomListener(div, "click", function() {
			google.maps.event.trigger(marker, "click");
		});
		this.div_ = div;
		const panes = this.getPanes();
		panes.overlayMouseTarget.appendChild(this.div_);
	};
	this.setMap(map);
	this.getPosition = function() {
		return coord;
	};
	this.remove = function(/*event*/) {
		this.div_.remove();
	};
}

CustomMarker.prototype = new google.maps.OverlayView() || $.noop;

class ContactMaps {
	constructor(officeCoord, routeSetting, checkSizeBlock) {
		this.officeCoord = officeCoord;
		this.routeSetting = routeSetting;
		this.checkSizeBlock = checkSizeBlock;
		this.gmaps = new GMaps({
			el: "#contactsMap",
			center: this.officeCoord,
			disableDefaultUI: true,
			zoomControl: true,
			zoom: 16,
			styles: APP_CONFIGS.gmapsStyles
		});
		this.lineSymbol = {
			path: "M 0,-1 0,1",
			strokeOpacity: 1,
			strokeColor: "#c43f60",
			strokeWeight: 6,
			scale: 4
		};
		this.showType = "office";
		$(window).resize(() => {
			this.setCheckSize();
		});
		this.setCheckSize();
		new CustomMarker(this.gmaps.map, "/img/map_markers/office_contacts.png", "Офис продаж", this.officeCoord);
	}

	setCheckSize() {
		this.checkSize = parseFloat(this.checkSizeBlock.width()) + parseFloat(this.checkSizeBlock.css("right"));
		if (window.innerWidth < 768) {
			this.checkSize = 0;
		}
		if (this.showType == "office") {
			this.setCenterOffice();
		} else {
			this.fitBoundsWithPadding(this.gmaps.map, [this.officeCoord].concat(this.routeSetting.map(item => item.coord)), {
				right: this.checkSize
			});
		}
	}

	fitBoundsWithPadding(map, coords, paddingXY) {
		var bounds = new google.maps.LatLngBounds();
		coords.forEach(item => bounds.extend(item));
		var projection = map.getProjection();
		if (projection) {
			if (!$.isPlainObject(paddingXY)) paddingXY = { x: 0, y: 0 };

			var paddings = {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			};

			if (paddingXY.left) {
				paddings.left = paddingXY.left;
			} else if (paddingXY.x) {
				paddings.left = paddingXY.x;
				paddings.right = paddingXY.x;
			}

			if (paddingXY.right) {
				paddings.right = paddingXY.right;
			}

			if (paddingXY.top) {
				paddings.top = paddingXY.top;
			} else if (paddingXY.y) {
				paddings.top = paddingXY.y;
				paddings.bottom = paddingXY.y;
			}

			if (paddingXY.bottom) {
				paddings.bottom = paddingXY.bottom;
			}

			// copying the bounds object, since we will extend it
			bounds = new google.maps.LatLngBounds(bounds.getSouthWest(), bounds.getNorthEast());

			// SW
			var point1 = projection.fromLatLngToPoint(bounds.getSouthWest());
			map.fitBounds(bounds);

			var point2 = new google.maps.Point(
				(typeof paddings.left == "number" ? paddings.left : 0) / Math.pow(2, map.getZoom()) || 0,
				(typeof paddings.bottom == "number" ? paddings.bottom : 0) / Math.pow(2, map.getZoom()) || 0
			);

			var newPoint = projection.fromPointToLatLng(new google.maps.Point(point1.x - point2.x, point1.y + point2.y));

			bounds.extend(newPoint);

			// NE
			point1 = projection.fromLatLngToPoint(bounds.getNorthEast());
			point2 = new google.maps.Point(
				(typeof paddings.right == "number" ? paddings.right : 0) / Math.pow(2, map.getZoom()) || 0,
				(typeof paddings.top == "number" ? paddings.top : 0) / Math.pow(2, map.getZoom()) || 0
			);
			newPoint = projection.fromPointToLatLng(new google.maps.Point(point1.x + point2.x, point1.y - point2.y));

			bounds.extend(newPoint);

			map.fitBounds(bounds);
		}
	}

	showRoute() {
		this.showType = "route";
		this.routeSetting.forEach(item => {
			if (item.type !== "walk") {
				this.gmaps.drawRoute({
					origin: [parseFloat(item.coord.lat()), parseFloat(item.coord.lng())],
					destination: this.endTransportPoint,
					travelMode: "driving",
					strokeColor: "#9893c0",
					strokeOpacity: 1,
					strokeWeight: 6
				});
			} else {
				this.endTransportPoint = [parseFloat(item.coord.lat()), parseFloat(item.coord.lng())];
				let directionsService = new google.maps.DirectionsService();
				directionsService.route(
					{
						origin: item.coord,
						destination: this.officeCoord,
						travelMode: google.maps.DirectionsTravelMode.WALKING,
						optimizeWaypoints: false
					},
					(result /*, status*/) => {
						this.gmaps.walkRoute = new google.maps.Polyline({
							path: result.routes[0].overview_path,
							strokeOpacity: 0,
							icons: [
								{
									icon: this.lineSymbol,
									offset: "0",
									repeat: "25px"
								}
							],
							map: this.gmaps.map
						})
					}
				);
			}
		});
		this.fitBoundsWithPadding(this.gmaps.map, [this.officeCoord].concat(this.routeSetting.map(item => item.coord)), {
			right: this.checkSize
		});
	}

	setCenterOffice() {
		this.showType = "office";
		this.gmaps.map.setCenter(this.officeCoord);
		this.gmaps.map.setZoom(16);
		this.gmaps.map.panBy(this.checkSize / 2, 0);
	}

	hideRoute() {
		this.gmaps.cleanRoute();
		if (this.gmaps.walkRoute != undefined) this.gmaps.walkRoute.setMap(null);
	}
}
