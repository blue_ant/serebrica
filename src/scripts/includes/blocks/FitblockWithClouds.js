class FitblockWithClouds {
	constructor(el) {
		this.outer = el;
		this.$el = $(el);
		this.inner = el.getElementsByClassName("Fitblock_inner")[0];
		this.pusher = this.inner.getElementsByClassName("Fitblock_pusher")[0];
		this.cloud = this.inner.getElementsByClassName("Fitblock_cloud")[0];

		let $window = $(window);

		$window.on(
			"resize.fitblocks",
			_.debounce(() => {
				this.refresh();
			}, 80)
		);

		if (this.pusher.complete || this.pusher.readyState === 4) {
			this.inner.style.visibility = "visible";
			this.refresh();
		} else {
			$(this.pusher).on("load", () => {
				this.inner.style.visibility = "visible";
				this.refresh();
			});
		}

		this.initAnimate(this.inner, this.cloud, this.pusher, $window);
	}

	refresh() {
		let outer = this.outer;
		let inner = this.inner;
		let pusher = this.pusher;
		let cloud = this.cloud;
		let wrapH = outer.offsetHeight;
		let wrapW = outer.offsetWidth;

		let pusherH = pusher.naturalHeight;
		let pusherW = pusher.naturalWidth;

		let rel = pusherW / pusherH;

		if (wrapW / pusherW > wrapH / pusherH) {
			pusher.style.width = `${wrapW + 50}px`;
			pusher.style.height = "auto";
			cloud.style.width = `${wrapW}px`;
			cloud.style.height = "auto";
			inner.style.width = `${wrapW + 50}px`;
			inner.style.marginLeft = `-${wrapW / 2}px`;
			inner.style.marginTop = `-${wrapW / rel / 2}px`;
		} else {
			pusher.style.width = "auto";
			pusher.style.height = `${wrapH + 50}px`;
			cloud.style.width = "auto";
			cloud.style.height = `${wrapH}px`;
			inner.style.marginLeft = `-${(wrapH * rel) / 2}px`;
			inner.style.marginTop = `-${wrapH / 2}px`;
		}
	}

	initAnimate(inner, cloud, pusher, $window) {
		let $pusher = $(pusher);

		let centerX = $pusher.width() / 2;
		let centerY = $pusher.height() / 2;

		let koefX;
		let koefY;
		let shiftX;
		let shiftY;
		let shift = 50;

		let mouse = { x: 0, y: 0 };

		let easedPos = {
			x: window.innerWidth / 2,
			y: window.innerHeight / 2
		};

		$window.mousemove(function(e) {
			mouse.x = e.clientX;
			mouse.y = e.clientY;
		});

		let update = () => {
			TweenLite.to(easedPos, 1, {
				x: window.innerWidth / 2 - mouse.x,
				y: window.innerHeight / 2 - mouse.y,
				ease: Power0.easeOut
			});

			koefX = easedPos.x / centerX;
			koefY = easedPos.y / centerY;

			shiftX = (-25 + shift * koefX) / 2;
			shiftY = (-25 + shift * koefY) / 2;

			TweenLite.set([cloud, inner], {
				x: shiftX,
				y: shiftY
			});
		};

		TweenLite.ticker.addEventListener("tick", update);
	}

	destroy() {
		$(window).off(".fitblocks");
	}
}
