class CardsList {
	constructor(opts) {
		this.tpl = _.template(opts.tpl.replace(/\s{2,}/g, ""));
		this.additionalClassName = opts.additionalClassName;
	}

	renderCards(dataToRender) {
		dataToRender.declOfNum = this.declOfNum;
		let markup = this.tpl(dataToRender);

		let resultMarkup = `<div class="CardsList ${
			this.additionalClassName ? this.additionalClassName : ""
		}">${markup}</div>`;

		return resultMarkup;
	}

	declOfNum(number, titles) {
		let cases = [2, 0, 1, 1, 1, 2];
		return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
	}
}
