class InlineGallery {
	constructor(gallery, elFancy) {
		let settings = {
			gallery: {
				slidesPerView: "auto",
				spaceBetween: 45,
				centeredSlides: true,
				speed: 900,
				navigation: {
					nextEl: ".gallery__btn--next",
					prevEl: ".gallery__btn--prev"
				},
				breakpoints: {
					767: {
						slidesPerView: 1,
						spaceBetween: 48
					}
				}
			},
			galleryDetail: {
				slidesPerView: "auto",
				centeredSlides: true,
				speed: 900,
				spaceBetween: 48,
				navigation: {
					nextEl: ".gallery__btn--next",
					prevEl: ".gallery__btn--prev"
				},
				breakpoints: {
					1439: {
						spaceBetween: 49,
						slidesPerView: 1
					}
				},
				on: {
					init: function() {
						$(".gallery-count__total").text(this.slides.length);
					},
					slideChange: function() {
						$(".gallery-count__current").text(this.realIndex + 1);
					},
					beforeResize() {
						if (window.innerWidth >= 1440) {
							this.slides.css("width", "");
						}
					}
				}
			},
			fancybox: {
				openEffect: "none",
				closeEffect: "none",
				arrows: true,
				speed: 900,
				buttons: ["close"],
				baseClass: "gallery-fancybox",
				baseTpl: `<div class="fancybox-container" role="dialog" tabindex="-1">
					<div class="fancybox-bg"></div>
					<div class="fancybox-inner">
						<div class="fancybox-infobar">
						<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>&nbsp;<span>Фото</span>
						</div>
						<div class="fancybox-toolbar">{{buttons}}</div>
						<div class="fancybox-navigation">{{arrows}}</div>
						<div class="fancybox-stage"></div>
						<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>
					</div>
				</div>`
			}
		};
		if (gallery.length) {
			const isDetail = gallery.hasClass("gallery--detail");
			new Swiper(gallery, isDetail ? settings.galleryDetail : settings.gallery);
			if (isDetail) this.showPopupGallery(elFancy, settings.fancybox);
		}
	}
	showPopupGallery(el, options) {
		el.fancybox(options);
	}
}
