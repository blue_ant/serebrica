class Tbl {
	constructor(el, opts = {}) {
		this.$el = $(el);
		this.$heads = this.$el.find(".Tbl_thead").find(".Tbl_th");

		this.$heads.filter("[data-sort-by]").on("click", event => {
			event.preventDefault();
			let $th = $(event.currentTarget);
			let sortName = $th.data("sort-by");

			this.$heads.not($th).removeClass("Tbl_th-sortableActive Tbl_th-sortable");
			$th.addClass("Tbl_th-sortable").toggleClass("Tbl_th-sortableActive");
			let direction = $th.hasClass("Tbl_th-sortableActive") ? "desc" : "asc";

			if (opts.onSortChange) {
				opts.onSortChange(sortName, direction);
			}
		});
	}
}
