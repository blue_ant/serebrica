class Accordion {
	constructor(el, conf) {
		let $el = $(el);

		if (!$el.length) {
			console.error("Accordion class constructor cant't find target element!");
			return;
		}

		const setting = {
			icons: false,
			heightStyle: "content",
			collapsible: true,
			classes: {
				"ui-accordion-header": "",
				"ui-accordion-content": "",
				"ui-accordion-header-active": "Accordion_label-active"
			}
		};
		$el.accordion($.extend(true, setting, conf));

		this.$el = $el;
	}
}
