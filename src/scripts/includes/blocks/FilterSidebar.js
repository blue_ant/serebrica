class FilterSidebar {
	constructor(el) {
		let $el = $(el);

		if ($el.length === 0) {
			console.error('FilterSidebar constructor can\'t find given "el" in DOM!');
			return;
		}

		this.$headerLogoEl = $(".Header_logo");

		this.$scroller = $el.find(".FilterSidebar_scroll");

		this.isHorizontal = $el.hasClass('FilterSidebar-horizontal');

		this.isNotFixed = $el.hasClass('FilterSidebar-notFixed');

		if (!this.isNotFixed) {
			this.$headerLogoEl.addClass("Header_logo-fixed").appendTo("main");
		}

		if (!this.isHorizontal) {
			new PerfectScrollbar(this.$scroller.get(0), {
				suppressScrollX: true,
				swipeEasing: false,
				wheelPropagation: false
			});
		}

		if (!this.isHorizontal) {
			$el.on("touchmove", event => {
				event.preventDefault();
				return false;
			});
	
			if (currentDevice.desktop()) {
				$el.hover(
					() => {
						this.show();
					},
					() => {
						this.hide();
					}
				);
			} else {
				$el.find(".FilterSidebar_grip").on("click", event => {
					event.preventDefault();
					this.toggle();
				});
			}
		}

		this.$el = $el;
	}

	show() {
		this.$el.addClass("FilterSidebar-active");
		this.$headerLogoEl.addClass("Header_logo-notShadowed");
	}

	hide() {
		this.$el.removeClass("FilterSidebar-active");
		this.$headerLogoEl.removeClass("Header_logo-notShadowed");
	}

	toggle() {
		if (this.$el.hasClass("FilterSidebar-active")) {
			this.hide();
		} else {
			this.show();
		}
	}

	addToggler(selector) {
		$(selector).on("click", event => {
			event.preventDefault();
			this.toggle();
		});
	}
}
