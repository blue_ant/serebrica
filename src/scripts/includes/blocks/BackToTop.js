class BackToTop {
	constructor() {
		let $el = $('<a class="BackToTop compensate-for-scrollbar" href="#" title="к началу"></a>');
		let $window = $(window);

		$window.on(
			"scroll.BackToTopListenter",
			_.throttle(() => {
				if ($window.scrollTop() > 200) {
					$el.fadeIn();
				} else {
					$el.fadeOut();
				}
			}, 100)
		);

		$el
			.on("click", event => {
				event.preventDefault();
				$("html, body").animate({ scrollTop: 0 }, 600);
				return false;
			})
			.appendTo("body");
	}
}
