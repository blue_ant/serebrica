class DiarySliders {
	constructor(diary, diaryThumb) {
		let settings = {
			diary: {
				slidesPerView: "auto",
				spaceBetween: 45,
				centeredSlides: true,
				speed: 900,
				navigation: {
					nextEl: ".diary_btn-next",
					prevEl: ".diary_btn-prev"
				},
				breakpoints: {
					767: {
						slidesPerView: 1,
						spaceBetween: 48
					}
				},
				on: {
					resize: function() {
						if (window.innerWidth >= 1440) {
							this.slides.css("width", "");
						}
					}
				}
			},
			diarySub: {
				slidesPerView: "auto",
				centeredSlides: true,
				speed: 900,
				spaceBetween: 48,
				navigation: {
					nextEl: ".diary_btn-next",
					prevEl: ".diary_btn-prev"
				},
				breakpoints: {
					1439: {
						spaceBetween: 49,
						slidesPerView: 1
					}
				},
				on: {
					init: function() {
						$(".diaryTop_count-total").text(this.slides.length);
					},
					slideChange: function() {
						$(".diaryTop_count-current").text(this.realIndex + 1);
					},
					resize: function() {
						if (window.innerWidth >= 1440) {
							this.slides.css("width", "");
						}
					}
				}
			},
			diaryThumb: {
				slidesPerView: "auto",
				centeredSlides: true,
				speed: 900,
				slideToClickedSlide: true,
				spaceBetween: 50,
				freeMode: true
			}
		};
		if (diary.length) {
			const isSub = diary.hasClass("diary-sub");
			let diarySlider = new Swiper(diary, isSub ? settings.diarySub : settings.diary);
			if (!isSub) {
				let thumbDiary = new Swiper(diaryThumb, settings.diaryThumb);
				diarySlider.controller.control = thumbDiary;
				thumbDiary.controller.control = diarySlider;
			}
		}

		$(".diaryTop_select select")
			.selectmenu({
				appendTo: ".diaryTop_select"
			})
			.on("selectmenuchange", function(/*event, ui*/) {
				window.location =
					"/be_aware/dynamics-construction/" + $("select[name=type]").val() + "/" + $("select[name=year]").val() + "/";
			});
	}
}
