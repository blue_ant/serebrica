class Slideshow {
	constructor(el, opts = {}) {
		if (!el) {
			throw new Error("Slideshow class requires at least one argument!");
		}
		let $el = $(el);
		let defaultOptions = {
			speed: 1000,
			loop: true,
			effect: "fade",
			allowTouchMove: true,
			navigation: {
				prevEl: $el.find(".Slideshow_arrow-prev").get(0),
				nextEl: $el.find(".Slideshow_arrow-next").get(0)
			},
			fadeEffect: {
				crossFade: true
			},
			autoplay: {
				delay: 9000,
				disableOnInteraction: false
			}
		};

		let sliderInstance = new Swiper($el.find(".swiper-container").get(0), $.extend(defaultOptions, opts));

		this.$el = $el;
		this.sliderInstance = sliderInstance;
	}

	/*destroy() {
        this.sliderInstance.trigger('destroy.owl.carousel');
    }*/
}
