class Teaser {
	constructor(el) {
		let $el = $(el);
		if (currentDevice.desktop()) {
			$el.find(".Teaser_picWrap").hover3d({
				shine: false,
				selector: $el.find(".Teaser_picture"),
				sensitivity: 40,
				// invert planning in FF because of strange bag
				invert: true
			});
		}

		$el.find(".Teaser_picture-withGallery").on("click", function(event) {
			event.preventDefault();
			$.ajax({
				url: event.target.dataset.srcJson,
				dataType: "json"
			})
				.done(items => {
					$.fancybox.open(items, {
						loop: true
					});
				})
				.fail(function() {
					console.error("Can't get gallery data from server!");
				});
		});
	}
}
