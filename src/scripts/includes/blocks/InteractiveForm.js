$.validator.addMethod(
	"regex",
	function(value, element, regexp) {
		var re = new RegExp(regexp);
		return this.optional(element) || re.test(value);
	},
	"Поле заполнено не верно"
);

class InteractiveForm {
	constructor(el, opts = {}) {
		this.$form = $(el);

		let $form = this.$form;

		if ($form.data("action")) {
			$form.prop("action", $form.data("action"));
		}

		let $phoneIntputs = $form.find('[name="phone"], [type="tel"]');

		$phoneIntputs.inputmask({
			mask: "+7 999 999-99-99",
			showMaskOnHover: false
		});

		$form.find('[name="captcha"]').inputmask({
			mask: "99999",
			showMaskOnHover: false
		});

		let validatorOpts = {
			rules: {
				captcha: {
					required: true,
					regex: /\d\d\d\d\d/
				}
			},
			errorElement: "em",
			onfocusout: (el /*, event*/) => {
				$(el).valid();
			},

			focusCleanup: false,
			submitHandler: opts.submitHandler || this.standartFormHandler, //(form)=>{}
			errorPlacement: ($errorLabel, $el) => {
				if ($el.attr("name") === "agree") {
					return true;
				} else {
					$errorLabel.addClass("Form_hint Form_hint-error");
					$el.after($errorLabel);
					return true;
				}
			}
		};

		$phoneIntputs.each((index, el) => {
			validatorOpts.rules[el.name] = {
				required: !!el.required,
				regex: /\+7\s\d\d\d\s\d\d\d\-\d\d\-\d\d/
			};
		});

		if (opts.validatorParams) {
			$.extend(true, validatorOpts, opts.validatorParams);
		}

		if (opts.successBlockMod) {
			$.extend(true, opts, { successBlockMod: "default" });
		}

		this.opts = opts;
		this.validator = $form.validate(validatorOpts);
	}

	standartFormHandler(form) {
		let $form = $(form);

		window.pagePreloader.show();

		let dataToSend = $.extend(true, $form.serializeObject(), {
			Submit: 1,
			url: window.location.href
		});

		$.ajax({
			url: form.action,
			type: form.method,
			data: dataToSend
		})
			.done(response => {
				let errorCode = parseInt(response.code);
				if (errorCode === 0) {
					let successText =
						`<div class="Form_success">` +
						`<div class="Form_successPicwrap"><img src="/img/red_branch_with_bird.svg" alt="------"></div>` +
						`<div class="Form_successTxt ">${response.success}</div>` +
						`</div>`;
					window.requestAnimationFrame(() => {
						$form.hide().after(successText);
					});
				} else if (errorCode === 1) {
					let validator = $form.data("validator");
					$form
						.find('[name="captcha"]:first')
						.val("")
						.trigger("focus");

					validator.showErrors({
						captcha: response.error
					});
				} else {
					alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
				}
			})
			.always((/*response*/) => {
				window.pagePreloader.hide();
			});
	}

	destroy() {
		this.validator.destroy();
		this.$form.find("input").inputmask("remove");
	}
}
