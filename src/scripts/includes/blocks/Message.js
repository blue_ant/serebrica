class Message {
	constructor(el, opts = {}) {
		this.opts = opts;
		this.$el = $(el).eq(0);
		this.$el.find(".Message_closeBtn").on("click", event => {
			event.preventDefault();
			this.hide();
			return false;
		});
		this.$content = this.$el.find(".Message_content");
	}

	setContent(content) {
		let markup = `<div class="Message_title Message_title-redColor">${content.title}</div><div class="Message_text">${
			content.text
		}</div>`;
		this.$content.html(markup);
	}

	show() {
		this.$el.fadeIn();
	}

	hide() {
		if (this.opts.beforeHide) {
			this.opts.beforeHide();
		}
		this.$el.fadeOut(/*"400", () => {
            if (this.opts.afterHide) {
                this.opts.afterHide();
            }
        }*/);
	}
}
