class PopBnr {
	constructor(el, opts = {}) {
		this.$el = $(el);

		if (opts.showMode === "session") {
			// show once per session
			let wasShown = window.sessionStorage.getItem("wasBannerShown");
			if (!wasShown) {
				window.sessionStorage.setItem("wasBannerShown", "1");
				this.show();
			}
		} else {
			// show by default if 'showMode' is not setted
			this.show();
		}
	}

	show() {
		$.fancybox.open(this.$el, {
			smallBtn: true,
			touch: false,
			toolbar: false
		});
	}
}
