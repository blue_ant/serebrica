class ArticleBurger {
	constructor(element) {
		let $el = $(element);

		if (!$el.length) {
			console.error("ArticleBurger class constructor cant't find target element!");
			return;
		}

		this.$el = $el;
		this.scrollLinks();
	}

	scrollLinks() {
		const links = this.$el.find('[data-scrollTo]');

		links.on('click', function (e) {
			e.preventDefault();

			const dataLink = $(this).data('scrollto');
			const title = $(document).find(`[data-titlescroll="${dataLink}"]`);

			$('html, body').animate({
				scrollTop: title.offset().top
			}, 2000);
		})
	}
}