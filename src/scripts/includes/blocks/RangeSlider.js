class RangeSlider {
	constructor(el) {
		if (!el) {
			console.error('RangeSlider constructor can\'t find given "el" in DOM!');
			return;
		}

		let $el = $(el);

		let $inputs = $el.find("input");
		let $inpMin = $inputs.eq(0);
		let $inpMax = $inputs.eq(1);

		let inlineElOpts = $el.data("slider-opts");
		let counterType = inlineElOpts.counterType || "num";
		let minVal = inlineElOpts.min;
		let maxVal = inlineElOpts.max;

		let currentMinVal = parseInt($inpMin.val());
		let currentMaxVal = parseInt($inpMax.val());

		let $minValCounter = $el.find(".RangeSlider_minValViewEl");
		let $maxValCounter = $el.find(".RangeSlider_maxValViewEl");

		if (counterType === "date_quarters") {
			this.quarterPoints = this._getQuartersFromRange(minVal, maxVal);
			minVal = 0;
			maxVal = this.quarterPoints.length - 1;

			let currentValuesQuaters = this._getQuartersFromRange(currentMinVal, currentMaxVal);

			currentMinVal = _.findIndex(this.quarterPoints, (t) => {
				return currentValuesQuaters[0].utc === t.utc;
			});

			currentMaxVal = _.findIndex(this.quarterPoints, (t) => {
				return _.last(currentValuesQuaters).utc === t.utc;
			});
		}

		let options = {
			classes: {
				"ui-slider-handle": "RangeSlider_handle",
				"ui-slider-range": "RangeSlider_progress",
			},
			slide: (event, ui) => {
				// let min = ui.values[0];
				// let max = ui.values[1];

				// if (min * 1.05 >= max) {
				// 	return false;
				// }

				this.renderValuesToCounterEl(ui.values);
			},
			change: (event, ui) => {
				this.setValuesToInputs(ui.values[0], ui.values[1], !!event.originalEvent);
			},
			values: [currentMinVal, currentMaxVal],
			min: minVal,
			max: maxVal,
			step: inlineElOpts.step || 1,
			range: true,
		};

		let $track = $("<div class='RangeSlider_track'></div>");
		$track.slider(options).prependTo($el);

		if (counterType !== "date_quarters") {
			$minValCounter.add($maxValCounter).on("click", (event) => {
				let $counter = $(event.currentTarget);
				if ($counter.find("input").length || $counter.is("input")) return false;
				let $inp = $counter.hasClass("RangeSlider_minValViewEl") ? $inpMin : $inpMax;
				let $popupInp = $("<input type='text' class='RangeSlider_popupInp'>");

				$popupInp
					.val($inp.val())
					.inputmask("integer", {
						groupSeparator: " ",
						autoGroup: true,
						allowMinus: false,
						rightAlign: false,
					})
					.on("change", (event) => {
						event.preventDefault();

						let $tmpInp = $(event.currentTarget);
						let val = parseInt(
							$tmpInp
								.val()
								.split(" ")
								.join("")
						);

						if (isNaN(val)) {
							val = $inp.val();
						} else {
							$inp.val(val);
						}

						let newVals = [parseInt($inpMin.val()), parseInt($inpMax.val())];

						if (newVals[0] > newVals[1]) {
							newVals = [newVals[1], newVals[0]];
						}

						if (newVals[0] < minVal) newVals[0] = minVal;
						if (newVals[1] > maxVal) newVals[1] = maxVal;

						-

						$track.slider("values", newVals);

						this.renderValuesToCounterEl(newVals);
						$tmpInp.remove();
						return false;
					})
					.on(
						"focusout",
						_.debounce(() => {
							$popupInp.remove();
							$('body').removeClass('keyboard-detect');
						}, 80)
					);

				$counter.parent().append($popupInp);
				$popupInp.trigger("focus");

				$('body').addClass('keyboard-detect');
			});
		}
		this.$minValCounter = $minValCounter;
		this.$maxValCounter = $maxValCounter;

		this.$inpMin = $inpMin;
		this.$inpMax = $inpMax;

		this.minMaxValues = [minVal, maxVal];

		this.counterType = counterType;

		this.$track = $track;

		this.renderValuesToCounterEl($track.slider("values"));
	}

	setValuesToInputs(min, max, fireChangeEvent) {
		if (this.counterType === "date_quarters") {
			min = this.quarterPoints[min].utc;
			max = this.quarterPoints[max].utc;
		}
		this.$inpMin.val(min);
		this.$inpMax.val(max);

		if (fireChangeEvent) {
			this.$inpMax.trigger("change");
		}
	}

	_getQuartersFromRange(start, end) {
		let startDate = new Date(start * 1000);
		let endDate = new Date(end * 1000);
		let result = [];

		let quarterPoints = [moment(startDate).unix()];
		let tmpQuarterPoint = startDate;

		while (tmpQuarterPoint < endDate) {
			tmpQuarterPoint = moment(tmpQuarterPoint)
				.add(3, "months")
				.endOf("quarter")
				.toDate();
			quarterPoints.push(moment(tmpQuarterPoint).unix());
		}

		let fmtTpl = "Q кв. YYYY";

		let titles = [];

		for (let i = 0; i < quarterPoints.length; i++) {
			let romanNumbers = ["I", "II", "III", "IV"];

			let titleStr = moment.unix(quarterPoints[i]).format(fmtTpl);
			let quartIndex = parseInt(titleStr) - 1;
			titleStr = romanNumbers[quartIndex] + titleStr.slice(1, titleStr.lenght);
			titles.push(titleStr);
		}

		for (var i = 0; i < titles.length; i++) {
			result.push({
				utc: quarterPoints[i],
				title: titles[i],
			});
		}
		return result;
	}

	renderValuesToCounterEl(vals) {
		if (this.counterType) {
			switch (this.counterType) {
				case "formatted_num":
					vals = _.mapValues(vals, (item) => {
						return item.toLocaleString("ru-RU");
					});

					break;

				case "date_quarters":
					vals = _.mapValues(vals, (item) => {
						return this.quarterPoints[item].title;
					});
					break;
			}
		}

		window.requestAnimationFrame(() => {
			this.$minValCounter.html(vals[0]);
			this.$maxValCounter.html(vals[1]);
		});
	}

	updateRange(minMax, fireChangeEvent) {
		this.$track.slider("values", minMax);
		this.renderValuesToCounterEl(minMax);
		this.setValuesToInputs(minMax[0], minMax[1], fireChangeEvent);
	}

	reset() {
		this.updateRange(this.minMaxValues);
	}
}
