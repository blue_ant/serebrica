class CustomBurger {
	constructor(el) {
		this.ignoreHovers = false;
		let $el = $(el);
		let $paths = $el.find("svg").find("path");
		let $hoverPaths = $(`
        /*=require ../../../templates/includes/svg/menu_btn_2.svg */
        `).find("path");

		let $semiclosePaths = $(`
        /*=require ../../../templates/includes/svg/menu_btn_3.svg */
        `).find("path");

		let $closePaths = $(`
        /*=require ../../../templates/includes/svg/menu_btn_4.svg */
        `).find("path");
		let tl = new TimelineLite({ paused: true });

		tl.addLabel("animeStart")
			.to($paths.eq(0), 0.2, { morphSVG: $hoverPaths.eq(0), ease: Linear.easeNone }, "animeStart")
			.to($paths.eq(1), 0.2, { morphSVG: $hoverPaths.eq(1), ease: Linear.easeNone }, "animeStart")
			.to($paths.eq(2), 0.2, { morphSVG: $hoverPaths.eq(2), ease: Linear.easeNone }, "animeStart")
			.to($paths.eq(3), 0.2, { morphSVG: $hoverPaths.eq(3), ease: Linear.easeNone }, "animeStart")
			.to($paths.eq(4), 0.2, { morphSVG: $hoverPaths.eq(4), ease: Linear.easeNone }, "animeStart")
			.addPause()
			.addLabel("semiCloseStart")
			.to($paths.eq(0), 0.1, { morphSVG: $semiclosePaths.eq(0), ease: Linear.easeNone }, "semiCloseStart")
			.to($paths.eq(1), 0.1, { morphSVG: $semiclosePaths.eq(1), ease: Linear.easeNone }, "semiCloseStart")
			.to($paths.eq(2), 0.1, { morphSVG: $semiclosePaths.eq(2), ease: Linear.easeNone }, "semiCloseStart")
			.to($paths.eq(3), 0.1, { morphSVG: $semiclosePaths.eq(3), ease: Linear.easeNone }, "semiCloseStart")
			.to($paths.eq(4), 0.1, { morphSVG: $semiclosePaths.eq(4), ease: Linear.easeNone }, "semiCloseStart")
			.addLabel("closeStart")
			.to($paths.eq(0), 0.1, { morphSVG: $closePaths.eq(0), ease: Linear.easeNone }, "closeStart")
			.to($paths.eq(1), 0.1, { morphSVG: $closePaths.eq(0), ease: Linear.easeNone }, "closeStart")
			.to($paths.eq(2), 0.1, { morphSVG: $closePaths.eq(0), ease: Linear.easeNone }, "closeStart")
			.to($paths.eq(3), 0.1, { morphSVG: $closePaths.eq(1), ease: Linear.easeNone }, "closeStart")
			.to($paths.eq(4), 0.1, { morphSVG: $closePaths.eq(1), ease: Linear.easeNone }, "closeStart");

		$el.hover(
			() => {
				if (this.ignoreHovers) return false;
				tl.play();
			},
			() => {
				if (this.ignoreHovers) return false;
				tl.reverse();
			}
		);

		this.$el = $el;
		this.tl = tl;
	}

	drawClosedBurger() {
		this.ignoreHovers = true;
		this.tl.gotoAndPlay("semiCloseStart");
	}

	reset() {
		this.tl.gotoAndStop("animeStart");
		this.ignoreHovers = false;
	}
}
