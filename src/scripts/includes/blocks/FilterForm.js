class FilterForm {
	constructor(el, opts) {
		if (!el) {
			console.error('Filter class constructor requires "el" argument!');
			return;
		}

		let defaultOpts = {
			submitHandler: $.noop,
		};

		opts = $.extend(defaultOpts, opts);

		this.lastChangedField = null;
		const submitHandler = _.debounce(opts.submitHandler, 700);

		let $filterForm = $(el);
		this.$filterForm = $filterForm;

		this.restoreStateFromURL();

		if (!$filterForm.length) {
			console.error('FilterForm constructor can\'t find given "el" in DOM!');
			return;
		}

		let multicheckboxes = [];
		$filterForm.find(".Multycheckbox").each((index, el) => {
			multicheckboxes.push(new Multicheckbox(el));
		});

		let rangeSliders = [];
		$filterForm.find(".RangeSlider").each((index, el) => {
			rangeSliders.push(new RangeSlider(el));
		});

		let $inputs = $($filterForm.get(0).elements);

		$inputs.on("change", (event) => {
			event.preventDefault();
			this.lastChangedField = event.currentTarget.name;
			$filterForm.trigger("submit");
		});

		$filterForm
			.on("submit", (event) => {
				event.preventDefault();
				this.saveStateToURL();
				submitHandler(this);
				return false;
			})
			.trigger("submit");

		$filterForm.find(".FilterForm_groupToggler").on("click", (event) => {
			event.preventDefault();
			let $toggler = $(event.currentTarget);
			$toggler.toggleClass("FilterForm_groupToggler-active");

			let $group = filter.$filterForm.find($toggler.data("form-group-toggle"));
			$group.toggle();

			if ($group.is(":visible")) {
				sidebar.$scroller.animate({ scrollTop: $group.offset().top }, 500);
			}
		});

		this.$inputs = $inputs;
		this.rangeSliders = rangeSliders;
		this.multicheckboxes = multicheckboxes;
	}

	resetFilterForm() {
		this.$filterForm[0].reset();
		this.rangeSliders.forEach((rangeSliderInst) => {
			rangeSliderInst.reset();
		});

		this.multicheckboxes.forEach((multiCheckboxInst) => {
			multiCheckboxInst.reset();
		});
	}

	resetChecksGroup() {
		let $checksGroup = this.$filterForm.eq(0).find('.FilterForm_chksGroup-groupBlock');
		let multicheckboxes = [];
		$checksGroup.find(".Multycheckbox").each((index, el) => {
			multicheckboxes.push(new Multicheckbox(el));
		});

		multicheckboxes.forEach((multiCheckboxInst) => {
			multiCheckboxInst.reset();
		});
	}

	parseQueryString(string) {
		if (string === "" || string == null) return {};
		if (string.charAt(0) === "?") string = string.slice(1);
		var entries = string.split("&"),
			data0 = {},
			counters = {};
		for (var i = 0; i < entries.length; i++) {
			var entry = entries[i].split("=");
			var key5 = decodeURIComponent(entry[0]);
			var value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";
			if (value === "true") value = true;
			else if (value === "false") value = false;
			var levels = key5.split(/\]\[?|\[/);
			var cursor = data0;
			if (key5.indexOf("[") > -1) levels.pop();
			for (var j = 0; j < levels.length; j++) {
				var level = levels[j],
					nextLevel = levels[j + 1];
				var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
				var isValue = j === levels.length - 1;
				if (level === "") {
					var key5 = levels.slice(0, j).join();
					if (counters[key5] == null) counters[key5] = 0;
					level = counters[key5]++;
				}
				if (cursor[level] == null) {
					cursor[level] = isValue ? value : isNumber ? [] : {};
				}
				cursor = cursor[level];
			}
		}
		return data0;
	}

	saveStateToURL() {
        if ( $("#filterFormHorizon").hasClass("pantry-filter") )
            return;

		let queryStr = this.$filterForm.serialize();
		let newURL = location.pathname + "?" + queryStr;
		history.replaceState({}, document.title, newURL);
	}

	restoreStateFromURL() {
        if ( $("#filterFormHorizon").hasClass("pantry-filter") )
            return;

		let storedValues = this.parseQueryString(window.location.search);

		// console.log("restored form states:", storedValues);
		let $inps = $(this.$filterForm.get(0).elements);

		for (let p in storedValues) {
			if (storedValues.offset) {
				this.offset = storedValues.offset;
			}
			// TODO: optimize selector for filtering inputs
			$inps.filter(`[name='${p}'], [name='${p}[]']`).each((index, el) => {
				if (el.type && (el.type === "checkbox" || el.type === "radio")) {
					let isChecked = true;

					if (_.isArray(storedValues[p])) {
						isChecked = _.includes(storedValues[p], el.value);
					}

					el.checked = isChecked;
				} else if (el.tagName === "SELECT") {
					if (storedValues[p]) {
						for (var i = 0; i < el.options.length; i++) {
							let opt = el.options[i];
							if (storedValues[p].includes(opt.value)) {
								opt.selected = "selected";
							}
						}
					}
				} else {
					el.value = storedValues[p];
				}
			});
		}
	}
}
