class MapFilter {
	constructor(el, conf) {
		let $el = $(el);

		if (!$el.length) {
			console.error("Map class constructor cant't find target element!");
			return;
		}

        this.$el = $el;
        this.$map = $el.find('.MapFilter_map');
        this.initMap();
        this.$filterItems = $el.find('.MapFilter_filterItem');
    }
    
    initMap() {
        const infrasMap = new GMaps({
            el: this.$map.get(0),
            center: {
                lat: 55.82654485,
                lng: 37.23891061
            },
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            zoom: 16,
            styles: APP_CONFIGS.gmapsStyles
        });

        this.gmap = infrasMap;

        this.filterMarkers();
        
        this.$map.on("click", ".marker-hover", function() {
            let $el = $(this);
            if (!$el.hasClass("marker-active")) {
                $(".marker-active").removeClass("marker-active");
                $el.addClass("marker-active");
            } else {
                $el.removeClass("marker-active");
            }
        });
    }

    filterMarkers() {
        if (window.MapFilterObjsMarkers) {
            window.MapFilterObjsMarkers = _.sortBy(window.MapFilterObjsMarkers, function(o) {
                return o.coord[0];
            });

            window.markers = [];

            //добавляем базовые маркеры на карту
            window.MapFilterObjsMarkers.forEach(item => {
                clearMarkers();
                if (item.type === 'all') {
                    markers.push(new CustomMarker(this.gmap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover));
                }
            });
        }

        function clearMarkers() {
            $('.marker').each((i, e) => {
                $(e).remove();
            });

            window.markers = [];

        }

        let $filterItems = this.$el.find('.MapFilter_filterItem');
        window.filterField = ['all'];
        let gmap = this.gmap;
        this.$filterItems = $filterItems;
        this.clearMarkers = clearMarkers;
        let resetFilter = this.resetFilter();
        let $filter = this.$el.find('.MapFilter_filter');

        $filterItems.on('click', function (e) {
            e.preventDefault();
            clearMarkers();

            console.log(filterField)
            $(this).toggleClass('MapFilter_filterItem-active');

            if ($(this).hasClass('MapFilter_filterItem-active')) {
                //добавляем нужный тип маркеров
                window.filterField.push($(this).data('filtertype'))
            } else {
                //удаляем нужный тип маркеров
                window.filterField.splice(filterField.indexOf($(this).data('filtertype')), 1);
            }

            window.MapFilterObjsMarkers.forEach(item => {
                if (item.type === 'all') {
                    window.markers.push(new CustomMarker(gmap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover));
                }
            });

            //добавляем нужные маркеры на карту согласно фильтру
            window.MapFilterObjsMarkers.forEach(item => {
                
                window.filterField.forEach((i) => {
                    if (item.type === i && item.type !== 'all') {
                        window.markers.push(new CustomMarker(gmap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover));
                    }
                })
            });

        })


        const resetButton = this.$el.find('.MapFilter_reset');
        
        resetButton.on('click', () => {
            this.resetFilter();
        })

        

        $(window).on('load', function () {
            
            $filterItems.each((i, e) => {
                $(e).trigger('click');
            });

            //Хак, при триггере почему-то дублирует несколько одинаковых маркеров. Если найдете ошибку - уберите.
            setTimeout(() => {
                $filterItems.eq(1).trigger('click');
                $filterItems.eq(1).trigger('click');
            }, 300)
        })

        $('.MapFilter_close').on('click', () => {
            $filter.fadeOut(300)
        });

        $('.MapFilter_openBtn').on('click', () => {
            $filter.fadeIn(300)
        });
    }

    resetFilter() {
        
        this.$filterItems.removeClass('MapFilter_filterItem-active');
        this.clearMarkers();

        window.MapFilterObjsMarkers.forEach(item => {
            if (item.type === 'all') {
                window.markers.push(new CustomMarker(this.gmap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover));
            }
        });

        window.filterField = ['all']
    }
}