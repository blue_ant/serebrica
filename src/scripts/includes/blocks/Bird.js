class Bird {
	constructor($targetEl, opts = {}) {
		let $bird = $(".Bird").eq(0);
		const positionFrom = $bird.position();
		const positionTo = $targetEl.position();
		const fromY = positionFrom.top;
		const fromX = positionFrom.left;
		let toX = positionTo.left + $targetEl.width() / 2 - 26;
		let toY = positionTo.top + $targetEl.height() / 2 + 30;

		const $birdAnimated = $(".Bird-animate");

		this.tl = new TimelineLite({
			paused: true,
			onComplete() {
				TweenLite.to($birdAnimated, 0.3, {
					opacity: 0,
					onComplete() {
						$birdAnimated.css("display", "none");
					}
				});
				$targetEl.css("opacity", "1");
			}
		});
		this.tl.addLabel("bird");
		this.tl.to($bird, 0, { top: fromY, left: fromX });
		this.tl.fromTo($bird, 0.9, { opacity: 0 }, { opacity: 1 }, "+0.4");
		this.tl.to($bird, 0, { className: "+=Bird-flipHead" }, "+=0.2");
		this.tl.addLabel("fly", "+=0.3");
		this.tl.to(
			$bird,
			0.2,
			{
				opacity: 0,
				onComplete() {
					$bird.css("display", "none");
				}
			},
			"fly"
		);
		this.tl.fromTo($birdAnimated, 0.2, { opacity: 0 }, { opacity: 1 }, "fly");
		this.tl.to(
			{
				top: fromY,
				left: fromX
			},
			2,
			{
				top: toY,
				left: toX,
				ease: Power0.easeNone,
				autoCSS: false,
				onUpdate(tween) {
					let koef = 10;
					if (tween.time() >= 0.5) {
						koef = 15;
					} else if (tween.time() >= 1) {
						koef = 25;
					}
					if (tween.time() >= 1.5) {
						koef = 30;
					}
					$birdAnimated.css("top", tween.target.top - koef * Math.sin(tween.time() * 6.4));
					$birdAnimated.css("left", tween.target.left);
				},
				onUpdateParams: ["{self}"],
				onUpdateScope: this
			},
			"fly"
		);
	}
	startAnimation() {
		this.tl.play();
	}
}
