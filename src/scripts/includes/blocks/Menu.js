class MainMenu {
	constructor(el, opts = {}) {
		let $menu = $(el);
		if (!$menu.length) {
			console.error("Menu class constructor cant't find target element!");
			return;
		}

		let $menuHeads = $menu.find(".Menu_groupHead-withSubmenu");

		$menuHeads.on("click", event => {
			event.preventDefault();
			let $clickedHead = $(event.target);
			$menuHeads.not($clickedHead).removeClass("Menu_groupHead-submenuOpen");
			$clickedHead.toggleClass("Menu_groupHead-submenuOpen");
		});

		let $closeBtn = $menu.find(".Menu_closeBtn");

		$closeBtn.on("click", event => {
			event.preventDefault();
			this.hide();
		});

		this.$menu = $menu;
		this.$closeBtn = $menu.find(".Menu_closeBtn");
		this.opts = opts;
	}

	show() {
		if (this.opts.onShow) this.opts.onShow();
		setTimeout(() => {
			$.fancybox.open(this.$menu, {
				arrows: false,
				infobar: false,
				toolbar: false,
				buttons: [],
				smallBtn: false,
				touch: false,
				beforeClose: () => {
					if (this.opts.onHide) this.opts.onHide();
					window.requestAnimationFrame(() => {
						this.$menu.removeClass("Menu-active");
					});
				}
			});

			window.requestAnimationFrame(() => {
				this.$menu.addClass("Menu-active");
			});
		}, 100);
	}

	hide() {
		$.fancybox.close();
	}
}
