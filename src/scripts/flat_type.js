let sidebar = new FilterSidebar("#filterSidebar");
let $flatsListCont = $(".FlatListContainer");

let $linksToFilterPage = $(".StickyBtn-settings");
let initialFilterPageURL = $linksToFilterPage.attr("href");

$linksToFilterPage = $linksToFilterPage.add(`a[href='${initialFilterPageURL}']`);

let isFilterChanged = false;
let filter = new FilterForm("#filterForm", {
	submitHandler: (filterInst) => {
		let $filterForm = filterInst.$filterForm;
		let queryStr = $filterForm.serialize();
		let url = location.pathname + "?" + queryStr;
		$linksToFilterPage.attr("href", `${initialFilterPageURL}?${queryStr}`);
		if (isFilterChanged) {
			$flatsListCont.load(`${url} .Tbl-flatList`);
		}
		isFilterChanged = true;
	},
});

sidebar.$el.find(".FilterSidebar_reset").on("click", (event) => {
	event.preventDefault();
	filter.resetFilterForm();
});
