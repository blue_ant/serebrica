const accordion = $(".contacts .Accordion");

new Accordion(accordion, {
	collapsible: true,
	active: false
});
$(".contacts_accordionContentWrp").each(function() {
	new PerfectScrollbar($(this)[0], {
		suppressScrollX: true,
		maxScrollbarLength: 25
	});
});
let fromCoords = [
	{
		type: "walk",
		coord: new google.maps.LatLng(55.82374, 37.247477)
	},
	{
		type: "transport",
		coord: new google.maps.LatLng(55.826183, 37.437014)
	},
	{
		type: "transport",
		coord: new google.maps.LatLng(55.850133, 37.439084)
	}
];

const contactMap = new ContactMaps(new google.maps.LatLng(55.8275052, 37.23578596), fromCoords, accordion);
/*
$(".contacts_accordionLabel").on("click", function() {
	if (this.dataset.type == "office") {
		contactMap.hideRoute();
		contactMap.setCenterOffice();
	} else {
		contactMap.showRoute();
	}
});*/
