let $transportMap = $("#transportMap");

let mapParams = {
	el: $transportMap.get(0),
	center: APP_CONFIGS.buildingLatLng,
	disableDefaultUI: true,
	zoomControl: true,
	zoom: 16,
	styles: APP_CONFIGS.gmapsStyles
};

let windowBreakpoint = getWindowSizeBreakpoint();

if (windowBreakpoint === "md") {
	mapParams.center = {
		lat: APP_CONFIGS.buildingLatLng.lat,
		lng: APP_CONFIGS.buildingLatLng.lng + 0.0075
	};
} else if (windowBreakpoint === "sm") {
	mapParams.zoom = 14;
	mapParams.center = {
		lat: APP_CONFIGS.buildingLatLng.lat + 0.003,
		lng: APP_CONFIGS.buildingLatLng.lng + 0.009
	};
} else if (windowBreakpoint === "xs") {
	mapParams.center = {
		lat: APP_CONFIGS.buildingLatLng.lat + 0.0033,
		lng: APP_CONFIGS.buildingLatLng.lng + 0.006
	};
	mapParams.zoom = 14;
}

const transportMap = new GMaps(mapParams);

{
	var path = [
		[55.8310593769994, 37.233914198105055],
		[55.83029851863924, 37.2379589692995],
		[55.82961011011211, 37.2413814680025],
		[55.82732740479738, 37.23936444682331],
		[55.82806416565615, 37.23661786479206],
		[55.82706168431912, 37.23550206584191],
		[55.82826948997036, 37.231704057876826],
		[55.8310593769994, 37.233914198105055]
	];

	transportMap.drawPolygon({
		paths: path,
		strokeColor: "#c43f60",
		strokeOpacity: 1,
		strokeWeight: 2,
		fillColor: "#c43f60",
		fillOpacity: 0.3
	});
}

if (window.infrasObjsMarkers) {
	window.infrasObjsMarkers = _.sortBy(window.infrasObjsMarkers, function(o) {
		return o.coord[0];
	});
	window.infrasObjsMarkers.forEach(item => {
		new CustomMarker(transportMap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover);
	});
}

$transportMap.on("click", ".marker-hover", function() {
	let $el = $(this);
	if (!$el.hasClass("marker-active")) {
		$(".marker-active").removeClass("marker-active");
		$el.addClass("marker-active");
	} else {
		$el.removeClass("marker-active");
	}
});

let messageBox = new Message(".Message", {
	beforeHide: () => {
		transportMap.cleanRoute();

		let optimalZoom = transportMap.getZoom() + 1;
		if (optimalZoom > 16) {
			optimalZoom = 16;
		}
		transportMap.setCenter(APP_CONFIGS.buildingLatLng);
		transportMap.setZoom(optimalZoom);
	}
});

$("#listsOfRoutes")
	.find("a")
	.on("click", function(event) {
		event.preventDefault();
		let $link = $(event.currentTarget);
		let routeIndex = parseInt($link.attr("href").split("#route_")[1]);
		let routeData = window.routesDesctiptions[routeIndex];

		transportMap.cleanRoute();

		let dest = routeData.destination || [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng];
		let tm = routeData.travelMode || "driving";
		let waypts = [];
		if (routeData.waypoints && tm !== "transit") {
			for (var i = routeData.waypoints.length - 1; i >= 0; i--) {
				waypts.push({
					location: routeData.waypoints[i]
				});
			}
		}

		// выводим маршрут следования, интервалами
		for (var ii = 1; ii < window.routesDesctiptions2[routeIndex].waypoints.length; ii++) {
			let start = window.routesDesctiptions2[routeIndex].waypoints[ii - 1];
			let finish = window.routesDesctiptions2[routeIndex].waypoints[ii];

			console.log("ii=" + ii + " mode=" + finish.mode);

			if (finish.mode == "WALKING") finish.mode = "walk";
			if (finish.mode == "TRANSIT") finish.mode = "transit";
			if (finish.mode == "DRIVING") finish.mode = "driving";

			if (finish.mode == "walk") {
				transportMap.drawRoute({
					origin: [start.lat, start.lng],
					destination: [finish.lat, finish.lng],
					strokeOpacity: 0,
					icons: [
						{
							icon: {
								path: "M 0,-1 0,1",
								strokeOpacity: 1,
								strokeColor: "#c43f60",
								strokeWeight: 3,
								scale: 4
							},
							offset: "0",
							repeat: "25px"
						}
					],
					// waypoints: [],
					travelMode: finish.mode
				});
			} else {
				transportMap.drawRoute({
					origin: [start.lat, start.lng],
					destination: [finish.lat, finish.lng],
					strokeColor: "#9893c0",
					strokeOpacity: 1,
					strokeWeight: 3,
					// waypoints: [],
					travelMode: finish.mode
				});
			}
		}

		transportMap.fitLatLngBounds([
			{
				lat: routeData.origin[0],
				lng: routeData.origin[1]
			},
			{
				lat: dest[0],
				lng: dest[1]
			}
		]);

		messageBox.setContent(routeData);
		messageBox.show();
		$("#showRoutesListBtn").trigger("click");
	});
