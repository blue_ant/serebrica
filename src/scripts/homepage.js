let bgSlider = new Slideshow($(".Slideshow").get(0));
let txtSlider = new Slideshow($(".Slideshow").get(1), { autoplay: false });

bgSlider.sliderInstance.controller.control = txtSlider.sliderInstance;
txtSlider.sliderInstance.controller.control = bgSlider.sliderInstance;

if (currentDevice.desktop() && $(window).width() > 767) {
	$(".Slideshow")
		.eq(1)
		.addClass("Slideshow-3Dlifted");
	$(".Content")
		.css("overflow", "visible")
		.hover3d({
			selector: ".FullScreenContainer",
			perspective: 2000,
			sensitivity: 500,
			// invert planning in FF because of strange bag
			invert: true
		});
}
