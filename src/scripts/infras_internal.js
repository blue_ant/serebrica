new FitblockWithClouds(document.querySelector(".Fitblock"));
new FilterSidebar("#filterSidebar");

let filterSLide = {
	init: function() {
		let $title = $(".FilterSidebar_zoneTitle-slide");

		$title.on("click", function() {
			$(this)
				.siblings(".FilterSidebar_zoneList")
				.slideToggle(0);
			$(this).toggleClass("FilterSidebar_zoneTitle-slideToggle");
		});
	}
};

filterSLide.init();

let filterControls = {
	init: function() {
		let controls = $(".FilterSidebar_zoneItem"),
			map = $(".SvgWrap_svg-infra"),
			img = $(".InfrasInternal"),
			reset = $(".FilterSidebar_reset");

		controls.on("click", function() {
			let name = $(this).data("zonename");
			$(this).toggleClass("FilterSidebar_zoneItem-active");

			if (name == "living") {
				map.find(".SvgWrap_infraLiving > .SvgWrap_infraPath").toggleClass("SvgWrap_infraPath-visible");
				img.find("[data-zoneIcon=" + name + "]").toggleClass("InfrasInternal_marker-visible");
			} else {
				map.find("#" + name + " > .SvgWrap_infraPath").toggleClass("SvgWrap_infraPath-visible");
				img.find("[data-zoneIcon=" + name + "]").toggleClass("InfrasInternal_marker-visible");
			}
		});

		reset.on("click", function() {
			controls.removeClass("FilterSidebar_zoneItem-active");
			map.find(".SvgWrap_infraPath-visible").removeClass("SvgWrap_infraPath-visible");
			img.find("[data-zoneIcon]").removeClass("InfrasInternal_marker-visible");
		});
	}
};

filterControls.init();
