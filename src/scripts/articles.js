$(".articles__select").each(function(index, el) {
	let $selectWrap = $(el);
	$selectWrap.find("select").selectmenu({
		appendTo: $selectWrap
	});
});

let searchBtn = $(".articles__search-field");

$(document).on("click", function(e) {
	if ((searchBtn.is(e.target) || $(".articles__search-btn").is(e.target)) && window.innerWidth < 1440) {
		searchBtn.parent().addClass("articles__search--active");
	} else {
		searchBtn.parent().removeClass("articles__search--active");
	}
});
