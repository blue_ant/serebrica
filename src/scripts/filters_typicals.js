let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find("#FilterSidebarResetFull").on("click", (event) => {
	event.preventDefault();
	filter.resetFilterForm();
	CorpusSchemeTrigger.reset();
});

sidebar.$el.find("#FilterSidebarResetGroup").on("click", (event) => {
	event.preventDefault();
	filter.resetChecksGroup();
});

$(".FilterMobileSwitcher").selectmenu({
	change: (event, ui) => {
		window.location.href = ui.item.value;
	},
});

let cardTplStr = `
/*=require ./includes/chunks/FlatCard.tpl */
`;

let plansListBuilder = new CardsList({
	tpl: cardTplStr,
	additionalClassName: "CardsList-flatPlans",
});

let filterParams = {
	submitHandler: (filterInst) => {
		let $filterForm = filterInst.$filterForm;
		let currentFiltersParams = $filterForm.serializeObject();

		// if (filterInst.lastChangedField === "bedrooms[]") {
		// 	delete currentFiltersParams.price_to;
		// 	delete currentFiltersParams.price_from;
		// 	delete currentFiltersParams.floor_to;
		// 	delete currentFiltersParams.floor_from;
		// 	delete currentFiltersParams.area_to;
		// 	delete currentFiltersParams.area_from;
		// }

		$.ajax({
			url: $filterForm.attr("action") || $filterForm.data("action"),
			dataType: "json",
			method: "GET",
			beforeSend: function() {
				if(window.pagePreloader.show) {
					window.pagePreloader.show();
				}
			},
			complete: function() {
				if (window.pagePreloader.hide) {
					window.pagePreloader.hide();
				}
			},
			// localCache: false,
			// cacheTTL: 1,
			// cacheKey: 'typicalsJSONCache',
			data: currentFiltersParams,
		})
			.done((jsonResponse) => {
				// parse filters params to object

				let filteredPlans = jsonResponse.objs;

				// if (filterInst.lastChangedField === "bedrooms[]") {
				// 	// set actual min and max range sliders values
				// 	let pricesAll = [];
				// 	let floorsAll = [];
				// 	let footagesAll = [];

				// 	_.each(filteredPlans, (pln) => {
				// 		_.each(pln.flats, (flat) => {
				// 			pricesAll.push(flat.price);
				// 			floorsAll.push(flat.floor);
				// 			footagesAll.push(flat.area);
				// 		});
				// 	});
				// 	if (pricesAll.length !== 0) {
				// 		_.each(filterInst.rangeSliders, (sldInst) => {
				// 			switch (sldInst.$inpMax.attr("name")) {
				// 				case "price_to":
				// 					sldInst.updateRange([_.min(pricesAll), _.max(pricesAll)]);
				// 					break;
				// 				case "floor_to":
				// 					sldInst.updateRange([_.min(floorsAll), _.max(floorsAll)]);
				// 					break;
				// 				case "area_to":
				// 					sldInst.updateRange([_.min(footagesAll), _.max(footagesAll)]);
				// 					break;
				// 			}
				// 		});

				// 		filterInst.saveStateToURL();
				// 	}
				// }

				_.each(filteredPlans, function(pln) {
					const flats = pln.flats;
					let areaMin = 0;
					let priceMin = 0;

					_.each(flats, (flat) => {
						if (areaMin < flat.area) {
							areaMin = flat.area;
						}

						if (priceMin < flat.price) {
							priceMin = flat.price;
						}
					});

					pln.areaMin = areaMin;
					pln.priceMin = priceMin;
				});

				{
					let urlSuffix = $.param(currentFiltersParams);

					_.forEach(filteredPlans, (plan) => {
						plan.href += "?" + urlSuffix;
					});
				}

				// fix pics urls in development enviroment
				if (window.location.href.indexOf("/filters_typicals.html") != -1) {
					console.warn("rewrite json response for dev enviroment usage...");
					filteredPlans.forEach((fltType) => {
						fltType.plan = "https://xn--80ablaq9bcd5c.xn--p1ai/" + fltType.plan;
						fltType.href = "/flat_type.html";
					});
					console.log("jsonResponse", jsonResponse);
				}

				filteredPlans = _.groupBy(filteredPlans, "rooms");

				if (!Object.keys(filteredPlans).length) {
					$("#filterResult").html(
						`<div class="NoPlansFound"><p class="NoPlansFound_text">Планировки по заданным параметрам не найдены,<br class="hidden-xs"> пожалуйста измените параметры поиска.</p></div>`
					);
				} else {
					let dataToRender = {
						plans: filteredPlans,
						promos: jsonResponse.promos,
					};

					$("#filterResult").html(plansListBuilder.renderCards(dataToRender));
				}
			})
			.fail(() => {
				alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
			});
	},
};

let filter = new FilterForm("#filterFormHorizon", filterParams);

$(".FiltersNav_switch, .FiltersNav_link.FiltersNav_link-navigate").on("click", (event) => {
	event.preventDefault();
	$(".FiltersNav_switch").toggleClass("FiltersNav_switch-right");

	setTimeout(() => {
		location.href = $(event.currentTarget).attr("href");
	}, 700);
});

let isRadioCorpus = false;
let CorpusSchemeTrigger = {
	init: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		$item.on("click", function(e) {
			const $data = $(e.currentTarget).data("triggercorpus");
			e.preventDefault()
			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			$checkbox.each((i, e) => {
				if ($(e).data(`persist-id`) === $data) {
					$(e).trigger("click");

					if ($(e).is(":checked")) {
						$(e.currentTarget).addClass("SvgSectionNav_link-active");
					} else {
						$(e.currentTarget).removeClass("SvgSectionNav_link-active");
					}
				}
			})
		});

		$checkbox.on("change", function() {
			const data = $(this).data("persist-id");

			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			
			if ($(this).is(":checked")) {
				$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
			} else {
				$(`[data-triggercorpus=${data}]`).removeClass("SvgSectionNav_link-active");
			}
		});

		$(window).on("load", function() {

			if (isRadioCorpus) {
				$checkbox.find('[data-persist-id = "item_1"]').trigger('click');
			}

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		});
	},

	reset: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		if (isRadioCorpus) {
			$checkbox.find('[data-persist-id = "item_1"]').trigger('click');

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");
					$item.removeClass("SvgSectionNav_link-active");
					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		}

		if (!isRadioCorpus) {
			$item.removeClass("SvgSectionNav_link-active");
		}
	}
};

CorpusSchemeTrigger.init();
