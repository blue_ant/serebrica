let popupTpl = _.template(
	`<div class="Dialog" style="max-width: 400px; pointer-event:none;">
        <div class="Dialog_head"><%= title %></div>
        <div class="Dialog_content">
            <dl class="Dialog_dl">
                <dt class="Dialog_dt">Площадь:</dt>
                <dd class="Dialog_dd"><%= footage %> м<sup>2</sup></dd>
                <dt class="Dialog_dt">Цена:</dt>
                <dd class="Dialog_dd"><%= price.toLocaleString("Ru-ru") %>&nbsp;₽</dd>
                <% if(promoPrice){ %>
                    <dt class="Dialog_dt">Цена по акции:</dt>
                    <dd class="Dialog_dd"><%= promoPrice.toLocaleString("Ru-ru") %>&nbsp;₽</dd>
                <% }; %>
                <dt class="Dialog_dt">Отделка:</dt>
                <dd class="Dialog_dd">
                    <% if(furnish){ %>
                        да
                    <% } else{ %>
                        нет
                    <% }; %>
                </dd>
            </dl>
        </div>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {
	$(".BldScheme_link").hover(
		event => {
			let $item = $(event.currentTarget);
			$item.addClass("BldScheme_link-active");
		},
		event => {
			let $item = $(event.currentTarget);
			$item.removeClass("BldScheme_link-active");
		}
	);

	let $scheme = $(".BldScheme");

	if (currentDevice.desktop() && $scheme.length) {
		$scheme.tooltip({
			items: "[data-params]",
			track: true,
			hide: { duration: 0 },
			show: { duration: 0 },
			content: function() {
				let $el = $(this);
				let params = $el.data("params");
				let dataToRender = {
					title: null,
					price: null,
					footage: null,
					promoPrice: null,
					entryDate: null,
					furnish: null
				};
				_.assignIn(dataToRender, params);
				return popupTpl(dataToRender);
			}
		});
	}
};

$(window).on("load", () => {
	setupSectionHoverBehaviour();
});

$("main").on("click", ".BldNav_arrow", event => {
	event.preventDefault();
	let ref = event.currentTarget.href;
	let $schemeBlock = $(".FlatInfo");
	$schemeBlock.addClass("FlatInfo-disabled");
	$.ajax({
		url: ref,
		dataType: "html"
	})
		.done(response => {
			let $newDoc = $(response);

			let $newSectionInfo = $newDoc.find(".FlatInfo");
			$schemeBlock.html($newSectionInfo.html());
			setupSectionHoverBehaviour();

			let title = $newDoc.filter("title").html();
			document.title = title;
			history.replaceState({}, title, ref);
		})
		.fail(function() {
			alert("Не удалось получить данные о секции!/nПопробуйте позже.");
		})
		.always(function() {
			$schemeBlock.removeClass("FlatInfo-disabled");
		});
});
