// TODO: Make zipped map of related elements instead of sorting them in DOM
let setupHoverBehaviour = () => {
	let $houseMasks = $(".HouseMask_item");

	let $floorSchemeMasks = $(".BldScheme_link");
	$floorSchemeMasks
		.sort((a, b) => {
			let result;
			let n1 = parseInt(a.attributes.class.value.split("BldScheme_link-n")[1]);
			let n2 = parseInt(b.attributes.class.value.split("BldScheme_link-n")[1]);
			if (n1 > n2) {
				result = 1;
			} else if (n1 < n2) {
				result = -1;
			} else if (n1 === n2) {
				result = 0;
			}
			return result;
		})
		.appendTo($floorSchemeMasks.parent());

	$houseMasks.hover(
		event => {
			let $item = $(event.currentTarget);
			$item.addClass("HouseMask_item-active");

			$floorSchemeMasks.eq($item.index()).addClass("BldScheme_link-active");
		},
		event => {
			let $item = $(event.currentTarget);
			$item.removeClass("HouseMask_item-active");

			$floorSchemeMasks.eq($item.index()).removeClass("BldScheme_link-active");
		}
	);

	$floorSchemeMasks.hover(
		event => {
			let $item = $(event.currentTarget);
			$item.addClass("BldScheme_link-active");

			$houseMasks.eq($item.index()).addClass("HouseMask_item-active");
		},
		event => {
			let $item = $(event.currentTarget);
			$item.removeClass("BldScheme_link-active");

			$houseMasks.eq($item.index()).removeClass("HouseMask_item-active");
		}
	);
};

/*
let dropHoverBehaviour = () => {
    $(".HouseMask_item, .BldScheme_link").off("mouseenter mouseleave");
};
*/

setupHoverBehaviour();

$("main").on("click", ".BldNav_arrow", event => {
	event.preventDefault();
	let ref = event.currentTarget.href;
	let $houseBlock = $(".HouseInfo");
	$houseBlock.addClass("HouseInfo-disabled");
	$.ajax({
		url: ref,
		dataType: "html"
	})
		.done(response => {
			let $newDoc = $(response);

			let $newHouseInfo = $newDoc.find(".HouseInfo");
			$houseBlock.html($newHouseInfo.html());

			let title = $newDoc.filter("title").html();
			document.title = title;
			history.replaceState({}, title, ref);

			setupHoverBehaviour();
		})
		.fail(function() {
			alert("Не удалось получить данные об этаже!/nПопробуйте позже");
		})
		.always(function() {
			$houseBlock.removeClass("HouseInfo-disabled");
		});
});
