(() => {
	let $accordion = $(".Accordion");

	new Accordion($accordion);

	if (currentDevice.desktop()) {
		$accordion.find(".DocCard").addClass("DocCard-desktop");
	}
})();
