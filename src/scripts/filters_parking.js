let filter = new FilterForm("#filterForm");

let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find("#FilterSidebarResetFull").on("click", (event) => {
	event.preventDefault();
	filter.resetFilterForm();
	CorpusSchemeTrigger.reset();
});

sidebar.$el.find("#FilterSidebarResetGroup").on("click", (event) => {
	event.preventDefault();
	filter.resetChecksGroup();
});

sidebar.addToggler("#filtersAsideTogger");

$(".FilterMobileSwitcher").selectmenu({
	change: (event, ui) => {
		window.location.href = ui.item.value;
	}
});

let $scheme = $(".EstScheme");

// START: schemes contain pages only
if (currentDevice.desktop() && $scheme.length) {
	$scheme.on("click", ".EstScheme_cell", event => {
		event.preventDefault();
		let params = $(event.currentTarget).data("params");

		$dialogEl
			.dialog("close")
			.html(dialogTpl({ data: params }))
			.dialog("option", "title", params[0])
			.dialog("option", "position", {
				my: "left-2 center",
				at: `left+${event.clientX} top+${event.clientY}`,
				of: window
			})
			.dialog("open");
	});

	let dialogTpl = _.template(`
<dl class="Dialog_dl">
    <dt class="Dialog_dt">Площадь:</dt>
    <dd class="Dialog_dd"><%= data[1] %> м<sup>2</sup></dd>
    <dt class="Dialog_dt">Цена:</dt>
    <dd class="Dialog_dd"><%= data[2].toLocaleString("ru-RU") %> ₽</dd>
</dl>
<div class="Dialog_btnWrap">
    <a class="Btn Btn-size2 Btn-color2" data-fancybox="" href="#orderModal">Отправить заявку</a>
</div>`);

	let $dialogEl = $("<div></div>");

	$dialogEl.dialog({
		draggable: false,
		autoOpen: false,
		resizable: false,
		closeText: "Закрыть",
		classes: {
			"ui-dialog": "Dialog",
			"ui-dialog-titlebar": "Dialog_head",
			"ui-dialog-titlebar-close": "Dialog_closeBtn",
			"ui-dialog-content": "Dialog_content"
		}
	});
}
// END: schemes contain pages only


let isRadioCorpus = false;
let CorpusSchemeTrigger = {
	init: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		$item.on("click", function(e) {
			const $data = $(e.currentTarget).data("triggercorpus");
			e.preventDefault()
			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			$checkbox.each((i, e) => {
				if ($(e).data(`persist-id`) === $data) {
					$(e).trigger("click");

					if ($(e).is(":checked")) {
						$(e.currentTarget).addClass("SvgSectionNav_link-active");
					} else {
						$(e.currentTarget).removeClass("SvgSectionNav_link-active");
					}
				}
			})
		});

		$checkbox.on("change", function() {
			const data = $(this).data("persist-id");

			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			
			if ($(this).is(":checked")) {
				$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
			} else {
				$(`[data-triggercorpus=${data}]`).removeClass("SvgSectionNav_link-active");
			}
		});

		$(window).on("load", function() {

			if (isRadioCorpus) {
				$checkbox.find('[data-persist-id = "item_1"]').trigger('click');
			}

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		});
	},

	reset: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		if (isRadioCorpus) {
			$checkbox.find('[data-persist-id = "item_1"]').trigger('click');

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");
					$item.removeClass("SvgSectionNav_link-active");
					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		}

		if (!isRadioCorpus) {
			$item.removeClass("SvgSectionNav_link-active");
		}
	}
};

CorpusSchemeTrigger.init();