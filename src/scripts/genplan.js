new Fitblock(document.querySelector(".Fitblock"));

let popupTpl = _.template(
	`<div class="Dialog Dialog-genplan" style="max-width: 209px; pointer-event:none;">
        <div class="Dialog_head">Корпус <%= corpus %></div>
        <div class="Dialog_content">
            <dl class="Dialog_dl">
                <dt class="Dialog_dt">Студия:</dt>
                <dd class="Dialog_dd"><%= study %></dd>
                <dt class="Dialog_dt">1-комнатная:</dt>
                <dd class="Dialog_dd"><%= room1 %></dd>
                <dt class="Dialog_dt">2-комнатная:</dt>
				<dd class="Dialog_dd"><%= room2 %></dd>
				<dt class="Dialog_dt">3-комнатная:</dt>
                <dd class="Dialog_dd"><%= room3 %></dd>
            </dl>
        </div>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {

	let $scheme = $(".SvgWrap_svg-genplan");

	if (currentDevice.desktop() && $scheme.length) {
		$scheme.tooltip({
			items: "[data-params]",
			track: true,
			hide: { duration: 0 },
			show: { duration: 0 },
			content: function() {
				let $el = $(this);
				let params = $el.data("params");
				let dataToRender = {
                    study: null,
                    room1: null,
                    room2: null,
                    room3: null
				};
				_.assignIn(dataToRender, params);
				return popupTpl(dataToRender);
			}
		});
	}
};

$(window).on("load", () => {
	setupSectionHoverBehaviour();
});