(() => {
    let $totalCount = $("#favoritesCount");

    let $page = $("#favoritesPage");

    let $list = $page.find('.js-list');

    let $empty = $page.find('.js-empty');

    let changeTotal = (change) => {
        let count = parseInt($totalCount.text()) + change;
        $totalCount.text(count);
        if (!count) {
	        $list.addClass('hidden');
	        $empty.removeClass('hidden');
        }
    };

    let active = '_active';

    $(document).on('click', '.js-favorite-link', function (e) {
        e.preventDefault();
        let $this = $(this);
        let url = $this.attr('href');
        $.ajax({
            url,
        })
            .done(r => {
                $this.toggleClass(active);
                changeTotal($this.hasClass(active) ? 1 : -1);
                if ($page.length){
                    $this.parent().fadeOut(300, () => {
                        $this.parent().remove();
                    });
                }
            });
    });

})();
