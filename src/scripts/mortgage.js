$(document.querySelector(".BankList_showMore")).one("click", event => {
	event.preventDefault();
	window.requestAnimationFrame(() => {
		$(".BankList_tblBody").removeClass("BankList_tblBody-closed");
		$(event.currentTarget)
			.parent()
			.remove();
	});
});
