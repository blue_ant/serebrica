let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find("#FilterSidebarResetFull").on("click", (event) => {
	event.preventDefault();
	filter.resetFilterForm();
	CorpusSchemeTrigger.reset();
});

sidebar.$el.find("#FilterSidebarResetGroup").on("click", (event) => {
	event.preventDefault();
	filter.resetChecksGroup();
});

$(".FilterMobileSwitcher").selectmenu({
	change: (event, ui) => {
		window.location.href = ui.item.value;
	},
});

let flatsList = new FlatsList(".Tbl-flatList2", {
	renderElement: "#filterResultsContainer",
});

const LIMIT = 12;

let offset = 0;

let $getMoreFlatsBtn = $("#getMoreFlats");

let updateGetMoreBtn = (response) => {
	let flatsRemains = response.total - offset - LIMIT;
	let loadNextTimeCount = LIMIT;
	if (flatsRemains < LIMIT) {
		loadNextTimeCount = flatsRemains;
	}

	if (loadNextTimeCount <= 0) {
		$getMoreFlatsBtn.hide();
	} else {
		$getMoreFlatsBtn
			.show()
			.find("#extraFlatsCounter")
			.html(loadNextTimeCount);
	}
};

let filterParams = {
	submitHandler: (filterInst) => {
		let $filterForm = filterInst.$filterForm;
		// window.pagePreloader.show();
		offset = 0;
		let dataToSend = $filterForm.serializeObject();

		// if (filterInst.lastChangedField === "bedrooms[]") {
		// 	delete dataToSend.price_to;
		// 	delete dataToSend.price_from;
		// 	delete dataToSend.floor_to;
		// 	delete dataToSend.floor_from;
		// 	delete dataToSend.area_to;
		// 	delete dataToSend.area_from;
		// }

		$.extend(true, dataToSend, {
			action: "get_flats",
			limit: LIMIT,
			offset: 0,
		});

		$.ajax({
			url: $filterForm.attr("action") || $filterForm.data("action"),
			dataType: "json",
			method: "GET",
			beforeSend: function() {
				if(window.pagePreloader.show) {
					window.pagePreloader.show();
				}
			},
			complete: function() {
				if (window.pagePreloader.hide) {
					window.pagePreloader.hide();
				}
			},
			data: dataToSend,
		})
			.done((response) => {
				// if (filterInst.lastChangedField === "bedrooms[]") {
				// 	// set actual min and max range sliders values
				// 	if (response.flats.length !== 0) {
				// 		_.each(filterInst.rangeSliders, (sldInst) => {
				// 			switch (sldInst.$inpMax.attr("name")) {
				// 				case "price_to":
				// 					sldInst.updateRange(response.prices);
				// 					break;
				// 				case "floor_to":
				// 					sldInst.updateRange(response.floors);
				// 					break;
				// 				case "area_to":
				// 					sldInst.updateRange(response.areas);
				// 					break;
				// 			}
				// 		});

				// 		filterInst.saveStateToURL();
				// 	}
				// }

				// fix pics urls in development enviroment
				if (window.location.href.indexOf("/filters_options.html") != -1) {
					console.warn("rewrite json response for dev enviroment usage...");
					response.flats.forEach((flt) => {
						flt.pic = "https://xn--80ablaq9bcd5c.xn--p1ai/" + flt.pic;
						flt.url = "/flat_single.html";
					});
				}

				flatsList.render({ flats: response.flats });
				updateGetMoreBtn(response);
			})
			.fail(() => {
				alert("Не удалось получить данные с сервера! Попробуйте позже.");
			})
			.always(function() {
				// window.pagePreloader.hide();
			});
	},
};

let filter = new FilterForm("#filterFormHorizon", filterParams);

$getMoreFlatsBtn.on("click", (event) => {
	event.preventDefault();

	let dataToSend = filter.$filterForm.serializeObject();

	$.extend(true, dataToSend, {
		action: "get_flats",
		limit: LIMIT,
		offset: offset + LIMIT,
	});

	$.ajax({
		url: filter.$filterForm.attr("action") || filter.$filterForm.data("action"),
		dataType: "json",
		method: "GET",
		data: dataToSend,
	})
		.done((response) => {
			flatsList.append({ flats: response.flats });
			offset += LIMIT;
			updateGetMoreBtn(response);
		})
		.fail(() => {
			alert("Не удалось получить данные с сервера! Попробуйте позже.");
		});
});

let $sortInp = $("#sortInp");
let $sortOrderInp = $("#sortOrderInp");

let tbl = new Tbl(".Tbl-flatList2", {
	onSortChange: (sortName, direction) => {
		$sortInp.val(sortName);
		$sortOrderInp.val(direction).trigger("change");
	},
});

tbl.$el.tooltip({
	items: ".Tbl_thumb",
	position: { my: "center", at: "center" },
	content: function() {
		let el = this;
		return `<div class="Tbl_popup"><img src="${el.dataset.fullImg}" class="Tbl_popupImg"></div>`;
	},
});

$(".FiltersNav_switch, .FiltersNav_link-navigate").on("click", function(e) {
	$(".FiltersNav_switch").toggleClass("FiltersNav_switch-right");

	e.preventDefault();

	var href = $(this).attr("href"),
		timeout = 700;

	setTimeout(function() {
		location.href = href;
	}, timeout);
});

let isRadioCorpus = false;
let CorpusSchemeTrigger = {
	init: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		$item.on("click", function(e) {
			const $data = $(e.currentTarget).data("triggercorpus");
			e.preventDefault();
			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			$checkbox.each((i, e) => {
				if ($(e).data(`persist-id`) === $data) {
					$(e).trigger("click");

					if ($(e).is(":checked")) {
						$(e.currentTarget).addClass("SvgSectionNav_link-active");
					} else {
						$(e.currentTarget).removeClass("SvgSectionNav_link-active");
					}
				}
			});
		});

		$checkbox.on("change", function() {
			const data = $(this).data("persist-id");

			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}

			if ($(this).is(":checked")) {
				$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
			} else {
				$(`[data-triggercorpus=${data}]`).removeClass("SvgSectionNav_link-active");
			}
		});

		$(window).on("load", function() {
			if (isRadioCorpus) {
				$checkbox.find('[data-persist-id = "item_1"]').trigger("click");
			}

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		});
	},

	reset: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		if (isRadioCorpus) {
			$checkbox.find('[data-persist-id = "item_1"]').trigger("click");

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");
					$item.removeClass("SvgSectionNav_link-active");
					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		}

		if (!isRadioCorpus) {
			$item.removeClass("SvgSectionNav_link-active");
		}
	},
};

CorpusSchemeTrigger.init();
