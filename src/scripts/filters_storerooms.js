let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find("#FilterSidebarResetFull").on("click", (event) => {
	event.preventDefault();
	filter.resetFilterForm();
	CorpusSchemeTrigger.reset();
});

sidebar.$el.find("#FilterSidebarResetGroup").on("click", (event) => {
	event.preventDefault();
	filter.resetChecksGroup();
});

$(".FilterMobileSwitcher").selectmenu({
	change: (event, ui) => {
		window.location.href = ui.item.value;
	},
});

let filterParams = {
	submitHandler: (filterInst) => {
		let $filterForm = filterInst.$filterForm;
		// window.pagePreloader.show();

		let dataToSend = $filterForm.serializeObject();

		let new_url = "/genplan/pantry/korpus_"+ dataToSend.bnum +"/section_"+ dataToSend.snum +"/";
		history.replaceState({}, document.title, new_url);

		$.extend(true, dataToSend, {
			action: "get_flats",
		});

		$.ajax({
			url: new_url,
			dataType: "html",
			method: "GET",
			beforeSend: function() {
				if(window.pagePreloader.show) {
					window.pagePreloader.show();
				}
			},
			complete: function() {
				if (window.pagePreloader.hide) {
					window.pagePreloader.hide();
				}
			}
		})
			.done((response) => {
				let new_html = $(response), i = new_html.find("#filterResult");
				$("#filterResult").html( i.html() );
				setupSectionHoverBehaviour()
			})
			.fail(() => {
				alert("Не удалось получить данные с сервера! Попробуйте позже.");
			})
			.always(function() {
				// window.pagePreloader.hide();
			});
	},
};

let filter = new FilterForm("#filterFormHorizon", filterParams);

let popupTpl = _.template(
	`<div class="Dialog Dialog-storerooms" style="position: absolute; width: 246px; left: <%- x %>px; top: <%- y %>px;">
		<div class="Dialog_head"><%= title %></div>
		<div class="Dialog_content">
			<dl class="Dialog_dl">
				<dt class="Dialog_dt">Площадь:</dt>
				<dd class="Dialog_dd"><%= area %> м<sup>2</sup></dd>
				<dt class="Dialog_dt">Цена:</dt>
				<dd class="Dialog_dd"><%= price %> &nbsp;₽</dd>
			</dl>

			<a href="#orderModal" data-fancybox="" class=" Dialog_btnStore Btn Btn-color2">Отправить заявку</a>
		</div>
		<div class="Dialog_closeBtn Dialog_closeBtn-left"></div>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {
	let $scheme = $(".EstScheme");

	$scheme.find("[data-params]").on("click", function(e) {
		e.preventDefault();

		if (currentDevice.desktop() && $scheme.length) {
			let $el = $(this);
			let params = $el.data("params");

			$(".Dialog-storerooms").remove();

			$("body").append(
				popupTpl({
					x: e.pageX,
					y: e.pageY,
					area: params.area,
					title: params.title,
					price: params.price,
				})
			);
		}
	});

	$(document).on("click", ".Dialog_closeBtn", function() {
		$(this)
			.parent(".Dialog")
			.remove();
	});
};


function showSectionsForBldNum(bldNum) {
    // sectionsList[ bldNum ]
    $("input[name=snum]").each(function(){
        var secnum = parseInt( $(this).val() );

        if ( sectionsList[ bldNum ].indexOf( secnum ) == -1 ) {
            $(this).closest( ".CustomCheckbox" ).hide();
        } else {
            $(this).closest( ".CustomCheckbox" ).show();
        }
    });

    var curr_secnum = parseInt( $("input[name=snum]:checked").val() );
    if ( sectionsList[ bldNum ].indexOf( curr_secnum ) == -1 ) {
        $("input[name=snum][value="+ sectionsList[ bldNum ][ 0 ] +"]").prop("checked","checked");
    }
}

$(window).on("load", () => {
	setupSectionHoverBehaviour();

	$("input[name=bnum]").click(function() {
        if(window.pagePreloader.show) {
            window.pagePreloader.show();
        }

		showSectionsForBldNum( parseInt( $(this).val() ) );
	});


	console.log( "selBld==" + $("input[name=bnum]:checked").val() );

	showSectionsForBldNum($("input[name=bnum]:checked").val());
});


let isRadioCorpus = false;
let CorpusSchemeTrigger = {
	init: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		$item.on("click", function(e) {
			const $data = $(e.currentTarget).data("triggercorpus");
			e.preventDefault()
			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			$checkbox.each((i, e) => {
				if ($(e).data(`persist-id`) === $data) {
					$(e).trigger("click");

					if ($(e).is(":checked")) {
						$(e.currentTarget).addClass("SvgSectionNav_link-active");
					} else {
						$(e.currentTarget).removeClass("SvgSectionNav_link-active");
					}
				}
			})
		});

		$checkbox.on("change", function() {
			const data = $(this).data("persist-id");

			if (isRadioCorpus) {
				$item.removeClass("SvgSectionNav_link-active");
			}
			
			if ($(this).is(":checked")) {
				$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
			} else {
				$(`[data-triggercorpus=${data}]`).removeClass("SvgSectionNav_link-active");
			}
		});

		$(window).on("load", function() {

			if (isRadioCorpus) {
				$checkbox.find('[data-persist-id = "item_1"]').trigger('click');
			}

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");

					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		});
	},

	reset: function() {
		const $item = $(".SvgSectionNav_link");
		const $checkbox = $(".CustomCheckbox").find('[name="corpus[]"]');

		if (isRadioCorpus) {
			$checkbox.find('[data-persist-id = "item_1"]').trigger('click');

			$checkbox.each((i, e) => {
				if ($(e).is(":checked")) {
					const data = $(e).data("persist-id");
					$item.removeClass("SvgSectionNav_link-active");
					$(`[data-triggercorpus=${data}]`).addClass("SvgSectionNav_link-active");
				}
			});
		}

		if (!isRadioCorpus) {
			$item.removeClass("SvgSectionNav_link-active");
		}
	}
};

CorpusSchemeTrigger.init();