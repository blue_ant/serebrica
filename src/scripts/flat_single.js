let thumbSlider = new Swiper($(".FlatSingle_planThumb .swiper-container"), {
	slidesPerView: "auto",
	slideToClickedSlide: true,
	navigation: {
		nextEl: ".FlatSingle_planThumbBtn-next",
		prevEl: ".FlatSingle_planThumbBtn-prev"
	},
	breakpoints: {
		767: {
			slidesPerView: "auto",
			centeredSlides: true
		}
	},
	on: {
		init: function() {
			this.slides[0].classList.add("FlatSingle_planThumbItem-active");
			if (this.slides.length <= 3) {
				$(".FlatSingle_planThumbBtn").css("display", "none");
			}
		},
		click: function() {
			let clickedSlide = this.clickedSlide;
			if (clickedSlide) {
				$.each(this.slides, (key, schemeSlide) => {
					schemeSlide.classList.remove("FlatSingle_planThumbItem-active");
					if (schemeSlide === clickedSlide) {
						schemeSlider.slideTo(key, 400, false);
						this.slideTo(key, 400, false);
					}
				});
				clickedSlide.classList.add("FlatSingle_planThumbItem-active");
			}
		}
	}
});

let schemeSlider = new Swiper($(".FlatSingle_slider"), {
	slidesPerView: 1,
	effect: "fade",
	spaceBetween: 30,
	on: {
		slideChange: function() {
			let main = thumbSlider.slides[schemeSlider.activeIndex];
			$.each(thumbSlider.slides, (key, images) => {
				images.classList.remove("FlatSingle_planThumbItem-active");
				if (images === main) {
					schemeSlider.slideTo(key, 400, false);
					thumbSlider.slideTo(key, 400, false);
				}
			});
			main.classList.add("FlatSingle_planThumbItem-active");
		},
		init: function() {
			if (this.slides.length < 2) {
				$(".FlatSingle_planThumb").css("display", "none");
				this.allowSlidePrev = false;
				this.allowSlideNext = false;
			}
		}
	}
});

let previewSlider = new Swiper($(".FlatSingle_preview .swiper-container"), {
	slidesPerView: 3,
	navigation: {
		nextEl: ".FlatSingle_previewBtn-next",
		prevEl: ".FlatSingle_previewBtn-prev"
	},
	spaceBetween: 14,
	breakpoints: {
		1439: {
			spaceBetween: 19
		},
		767: {
			spaceBetween: 10
		}
	}
});

if (previewSlider.slides && previewSlider.slides.length < 4) {
	previewSlider.allowTouchMove = false;
}

$("#scrollToFootageLink").on("click", event => {
	event.preventDefault();
	$("html, body").animate({ scrollTop: $("#footageBlock").offset().top }, 600);
	return false;
});

{
	let $popupLink = $("#showFootagePopupLink");
	let $popContent = $("#footageBlock")
		.find(".Panel")
		.clone();

	$popContent.addClass("Panel-popup").css("min-width", "464px");

	$popupLink
		.on("click", function(event) {
			event.preventDefault();
			return false;
		})
		.tooltip({
			items: $popupLink,
			position: {
				my: "center top",
				at: "center bottom+18",
				of: $popupLink
			},
			content: $popContent,
			show: 200,
			hide: 200,
			tooltipClass: "ui-tooltip-auto-width"
		});
}

(function($toggle) {
	if (!$toggle.length) return;

	let active    = '_active';
	let $form     = $('.TechFormWrapper');
	let $close    = $form.find('.TechFormWrapper__close');
	let $checkbox = $toggle.find('.TechBtns__inp');

	$toggle.on('click', function() {
		if ($checkbox.is(':checked')) {
			$toggle.removeClass(active);
			$form.removeClass('_active').animate({
				height: 0
			}, 300, function() {
				clearForm($form.find('.TechForm'));
			});
			$checkbox
				.attr('checked', false)
				.prop('checked', false);

			return;
		}

		$toggle.addClass(active);
		$checkbox
			.attr('checked', true)
			.prop('checked', true);

		$form.addClass('_active').animate({
			height: $form.get(0).scrollHeight
		}, 300);
	});

	$close.on('click', function() {
		$toggle.removeClass(active);
		$form.animate({
			height: 0
		}, 300, function() {
			clearForm($form.find('.TechForm'));
		});
		$checkbox
			.attr('checked', false)
			.prop('checked', false);
	});
})($('[data-toggle-form]'));

function clearForm($form) {
	$form.removeClass('_submited');
	$form.removeClass('_submited').find('input[type="email"]').val('');
}

(function($form) {
	if (!$form.length) return;

	$form.on('submit', function(e) {
		e.preventDefault();

		let url    = $form.attr('action');
		let method = $form.attr('method');
		let data   = $form.serialize();

		$.ajax({
			url: $form.data("action"),
      type: "json",
      method: method,
      data: data,
      success: function() {
      	$form
      		.addClass('_submited')
      		.closest('.TechFormWrapper')
      		.animate({
      			height: $('.TechFormWrapper').get(0).scrollHeight
      		});
      }
		});
	});
})($('[data-follow-form]'));
