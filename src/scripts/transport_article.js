const map = new TransportMap(document.querySelector('.TransportMap'));
const metroScheme = new Fitblock(document.querySelector('.Fitblock-transportArticle'));
const metroSchemePerspective = new Fitblock(document.querySelector('.Fitblock-transportPerspective'));


$('.Teaser_videoPlay').on('click', function () {
    player.playVideo();
    let button = $(this);
    setTimeout(function () {
        button.fadeOut(300);
    }, 300)
});


let $zoomPicture = $('.ZoomPicture');

$zoomPicture.on('click', function (e) {
    e.preventDefault();
    $('.ZoomPictureOverlay').remove()
    let href = $(this).attr('href');
    let imgTpl = _.template(`<div class="ZoomPictureOverlay"><img class="ZoomPictureFull" src="${href}"/></div>`);
    $('body').append(imgTpl())
});

$(document).on('click', '.ZoomPictureOverlay', function () {
    $(this).fadeOut(300)
})

const $map = $('.TransportMap');
let counterBlink = 0;
let stopAnimateLink = false;
var linkBlink;

$(window).on('load', function () {

    var animateLink = requestAnimationFrame(()=> {
        requestAnimationFrame (() => {
            linkBlink = setInterval(() => {
                $map.find('li > a').eq(0).toggleClass('fade');
                counterBlink = counterBlink + 1;
            }, 800);
        })
    });

    $map.find('a').hover(function () {
        clearInterval(linkBlink);
        $map.find('li > a').eq(0).removeClass('fade');
    });

    $(window).on('scroll', function () {
        let mapPosition = $map.offset().top;
        let scrollposition = $(window).scrollTop();
        
        if (scrollposition > mapPosition + parseInt($map.height())) {
            clearInterval(linkBlink);
            $map.find('li > a').eq(0).removeClass('fade');
        }
    })
})